// import * as Rx from "rxjs";
const Rx = require("rxjs");
const of = require("rxjs/observable/of").of
const from = require("rxjs/observable/from").from
const interval = require("rxjs/observable/interval").interval

const map = require("rxjs/operators/map").map
const tap = require("rxjs/operators/tap").tap
const filter = require("rxjs/operators/filter").filter
const mergeMap = require("rxjs/operators/mergeMap").mergeMap
const mergeAll = require("rxjs/operators/mergeAll").mergeAll
const delay = require("rxjs/operators/delay").delay
const take = require("rxjs/operators/take").take
const ignoreElements = require("rxjs/operators/ignoreElements").ignoreElements
const pluck = require("rxjs/operators/pluck").pluck
const share = require("rxjs/operators/share").share

const _ = require('lodash');
// const { map, filter, scan } = require('rxjs/operators')

// const observable = Rx.Observable.create((observer) => {
//     observer.next(Math.random());
// });
//
// // subscription 1
// observable.subscribe((data) => {
//   console.log(data); // 0.24957144215097515 (random number)
// });
//
// // subscription 2
// observable.subscribe((data) => {
//    console.log(data); // 0.004617340049055896 (random number)
// });

/* ---------------------------------------------------------------- */

// const subject = new Rx.Subject();
//
// // subscriber 1
// subject.subscribe((data) => {
//     console.log(data); // 0.24957144215097515 (random number)
//     // console.log("magi");
// });
//
// // subscriber 2
// subject.subscribe((data) => {
//     console.log(data); // 0.24957144215097515 (random number)
//     setTimeout(() => {
//         console.log('Test');
//     }, 0.0000000001);
// });
//
// subject.next(Math.random());

/* ---------------------------------------------------------------- */

// const observable = Rx.Observable.create((observer) => {
//     observer.next(Math.random());
// });
//
// const subject = new Rx.Subject();
//
// // subscriber 1
// subject.subscribe((data) => {
//     console.log(data); // 0.24957144215097515 (random number)
// });
//
// // subscriber 2
// subject.subscribe((data) => {
//     console.log(data); // 0.24957144215097515 (random number)
// });
//
// observable.subscribe(subject);

/* ---------------------------------------------------------------- */

// lets create our data first
// data = of([
//   {
//     brand: 'porsche',
//     model: '911'
//   },
//   {
//     brand: 'porsche',
//     model: 'macan'
//   },
//   {
//     brand: 'ferarri',
//     model: '458'
//   },
//   {
//     brand: 'lamborghini',
//     model: 'urus'
//   }
// ]);
//
// data = data.pipe(
//     tap(values => values.map(val => console.log(val.model)))
// );
//
// // get data as brand+model string.
// data
//   .pipe(
//     map(cars => cars.map(car => `${car.brand} ${car.model}`))
//   ).subscribe(cars => console.log(cars))
//
// // filter data so that we only have porsches.
// data
//   .pipe(
//     map(cars => cars.filter(car => car.brand === 'porsche'))
//   ).subscribe(cars => console.log(cars))

/* --------------------------------------------------------- */


// const getData = (param) => {
//   return of(`retrieved new data with param ${param}`).pipe(
//     delay(1000)
//   )
// }
//
// console.log("using a regular map");
// from([1,2,3,4]).pipe(
//   map(param => getData(param))
// ).subscribe(val => console.log(val));  // così non va bene. val è ancora un observable. Usare questo: val.subscribe(data => console.log(data))
//
// console.log("using map and mergeAll");
// from([1,2,3,4]).pipe(
//   map(param => getData(param)),
//   mergeAll()
// ).subscribe(val => console.log(val));
//
// console.log("using mergeMap");
// from([1,2,3,4]).pipe(
//   mergeMap(param => getData(param))
// ).subscribe(val => console.log(val));


// obs = from(['a','b','c','d']);
//
// obs.subscribe(val => console.log(val));


/* -------------------------------------------------------------------- */

// const subject = new Rx.Subject(); // 0 is the initial value
//
// subject.subscribe({
//   next: (v) => console.log(`observerA: ${v}`)
// });
//
// subject.next(1);
// subject.next(2);
//
// subject.subscribe({
//   next: (v) => console.log(`observerB: ${v}`)
// });
//
// subject.next(3);

/* -------------------------------------------------------------------- */

// //emit value every 100ms
// const source = interval(100);
// //ignore everything but complete
// const example = source.pipe(
//   // take(5),
//   ignoreElements()
// );
// //output: "COMPLETE!"
// const subscribe = example.subscribe(
//   val => console.log(`NEXT: ${val}`),
//   val => console.log(`ERROR: ${val}`),
//   () => console.log('COMPLETE!')
// );

/* -------------------------------------------------------------------- */


// // simulate url change with subject
// const routeEnd = new Rx.Subject();
//
// // grab url and share with subscribers
// const lastUrl = routeEnd.pipe(
//   // pluck('url'),
//   share()
// );
//
// // initial subscriber required
// const initialSubscriber = lastUrl.subscribe(console.log);
//
// // simulate route change
// routeEnd.next('my-path');
//
// // nothing logged
// const lateSubscriber = lastUrl.subscribe(console.log);

/* ------------------------------------------------------------------ */
// let orig = [{
//         id: 1,
//         values: {
//             a: 'a',
//             b: 'b'
//         }
//     },
//     {
//         id: 2,
//         values: {
//             c: 'c',
//             d: 'd'
//         }
//     }
// ];
//
// let val;
// subj = new Rx.BehaviorSubject(undefined).pipe(
//     filter(val => val != undefined),
//     map(val => _.cloneDeep(val))
// );
// obs = subj.asObservable();
//
//
//
// obs.subscribe(_val => {
//     val = _val;
//     console.log(val);
// });
//
// subj.next(orig);

// subj.next({a:"second a", b:"second b"});

// setTimeout(() => console.log(subj.value), 10);

// val[0].values.a = "external a"
//
// // console.log(subj.value)
// obs.subscribe(_val => console.log(_val))

// var orig = [{
//         id: 1,
//         values: {
//             a: 'a',
//             b: 'b'
//         }
//     },
//     {
//         id: 2,
//         values: {
//             c: 'c',
//             d: 'd'
//         }
//     }
// ];
//
// let cloned = _.cloneDeep(orig)
// cloned[0].values.a = "xx";
//
// console.log(orig)
// console.log(cloned)
// console.log(orig == cloned);
// console.log(orig === cloned);
//
// cloned = orig;
// cloned.a = "xx";
//
// console.log(orig == cloned);
// console.log(orig === cloned);



// var a = {
//         b: 'hello',
//         c: {
//             d: 'world'
//         }
//     };
// var b = Object.create(a);
// a == b /* false */ ;
// a.c == b.c /* true */ ;



/* ------------------------------------------------------------- */

// function Foo() {
//   this.a = [1,2,3];
//   this.b = 1;
// }
//
// x = new Foo();
// y = {};
// y = _.cloneDeep(x);
// x.a[0] = 8;
// x.b = 2;
// console.log(x);
// console.log(y);

console.log('********');

arr = [{id:1, a:'a1', b:'b1'}, {id:2, a:'a2', b:'b2'}];

// console.log("index:", _.findIndex(arr, {'id': 2}))

console.log(arr)
let pulled = _.remove(arr, {'id': 2})
console.log(arr)
console.log(pulled)
console.log('********');

// arr = ['a','b','c','d','e','f']
//
// pulled = _.pullAt(arr, 3).reduce(val => val);
// console.log("pulled:",pulled);


/* ------------------------------------------------------------- */
//
// function Course(title, cfu, code) {
//     this.title = title;
//     this.cfu = cfu;
//     this.code = code;
// }
//
// function Pds(name, courses) {
//     this.name = name;
//     this.courses = courses;
// }
//
// var courses = [undefined, undefined, undefined, undefined, undefined];
//
// courses[0] = new Course("course0", 6, "000");
// courses[1] = new Course("course1", 6, "001");
// courses[2] = new Course("course2", 9, "002");
// courses[3] = new Course("course3", 9, "003");
// courses[4] = new Course("course4", 6, "004");
//
// var current_pds = new Pds("pds0", courses);
//
// updated_pds = _.cloneDeep(current_pds);
//
// updated_pds.courses[1].cfu = 9;
// updated_pds.courses[1].code = "001_9";
//
// console.log(updated_pds);
// console.log(current_pds);
//
// modified_fields = {}
// for (let prop of Object.keys(updated_pds)) {
//     if (!_.isEqual(updated_pds[prop], current_pds[prop])) {
//         console.log("prop not equal: ", prop);
//         console.log(updated_pds[prop]);
//         console.log(current_pds[prop]);
//         if (prop == "courses") {
//             console.log("prop == courses");
//         }
//         else {
//             modified_fields[prop] = updated_pds[prop];
//         }
//     }
// }
// console.log("modified_fields: ", modified_fields);
// // console.log("is empty: ", _.isEmpty(modified_fields));
// if (_.isEmpty(modified_fields)) {
//     console.log("NOT MODIFIED.");
// }

// error = {"username": "magimagi", "altrakey": "altrovalue"}
// for (let prop of Object.keys(error)) {
//     console.log(error[prop])
// }

raw_date = '2019-03-22T17:25:00.688155Z'
date = new Date(raw_date);
// var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
// var options = { year: 'numeric', day: 'numeric' };
//
// date.setSeconds(0,0)
console.log(date)
console.log(date.toLocaleString('it-IT'))  // .split(':').slice(0,-1).join(':')


console.log('****************')

a = [0,1,2,3,4,5,6,7,8,9]
console.log(a)
a.push(10)
console.log(a)
a.push(...[11,12])
console.log(a)

console.log('****************')

console.log("aaaaaaaabbbbaa" < "aaaaaaaabbbba")
