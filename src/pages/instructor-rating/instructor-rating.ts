import { Component, ViewChild } from "@angular/core";
import {
    Events,
    IonicPage,
    Loading,
    LoadingController,
    NavController,
    NavParams,
    ViewController
} from "ionic-angular";
import _ from "lodash";
import { BehaviorSubject } from "rxjs";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { Instructor } from "../../model/instructor/instructor.model";
import { InstructorRating } from "../../model/rating/rating.model";
import {
    RatingDataProvider
} from "../../providers/data/rating-data.provider";
import {
    InstructorProvider
} from "../../providers/instructor/instructor.provider";

@IonicPage()
@Component({
    selector: "page-instructor-rating",
    templateUrl: "instructor-rating.html",
})
export class InstructorRatingPage {

    @ViewChild("comment")
    private comment: any;

    @ViewChild("stars")
    private stars: any;

    private selected_instructor: BehaviorSubject<Instructor>;
    private ratings_subject: BehaviorSubject<any>;
    private existing_rating: InstructorRating;
    private loading: Loading;

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public view_ctrl: ViewController,
        public events: Events,
        private loading_ctrl: LoadingController,
        public rating_provider: RatingDataProvider,
        public instructor_provider: InstructorProvider) {

        this.events.subscribe("refreshTokenExpired",
            () => {
                this.view_ctrl.dismiss();
            });
    }

    ngOnInit() {
        this.selected_instructor = this.nav_params.get("instructor");
        this.ratings_subject = this.nav_params.get("rate_sbj");
        this.existing_rating = this.nav_params.get("rating");
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

    save_rating() {
        this.show_loading();
        let response = this.existing_rating ?
            this.update_rating() : this.create_new_rating();
        response.subscribe((resp) => {
            this.rating_provider.update_instructor_ratings(resp);
            this.loading.dismiss();
            this.view_ctrl.dismiss();
        });

    }

    update_rating(): Observable<any> {
        let current_ratings = this.ratings_subject.value;
        let current_rating_idx = _.findIndex(current_ratings,
            { owner: this.existing_rating.owner });
        // rate-update
        let new_stars = this.stars.rating;
        let ratings_num = current_ratings.length;
        let new_mean = ((this.selected_instructor.value.stars * ratings_num)
            + new_stars - this.existing_rating.stars) / ratings_num;
        let new_instructor = this.selected_instructor.value;
        new_instructor.stars = new_mean;

        // store change
        this.existing_rating.comment = this.comment.value;
        this.existing_rating.stars = new_stars;
        current_ratings[current_rating_idx] = this.existing_rating;

        return this.instructor_provider
            .update_instructor_rating(this.existing_rating)
            .pipe(tap(() => {
                this.selected_instructor.next(new_instructor);
                this.ratings_subject.next(current_ratings);
            }
            ));
    }

    create_new_rating(): Observable<any> {
        let new_rate = new InstructorRating({
            stars: this.stars.rating,
            comment: this.comment.value
        });
        // rate-update
        let new_stars = this.stars.rating;
        let current_ratings = this.ratings_subject.value;
        let ratings_num = current_ratings.length;
        let new_mean = ((this.selected_instructor.value.stars * ratings_num)
            + new_stars) / (ratings_num + 1);
        let new_instructor = this.selected_instructor.value;
        new_instructor.stars = new_mean;

        return this.instructor_provider
            .save_instructor_rating(this.selected_instructor.value.id, new_rate)
            .pipe(tap((resp) => {
                this.selected_instructor.next(new_instructor);
                const current_ratings = this.ratings_subject.value;
                const updated_ratings = [...current_ratings, resp];
                this.ratings_subject.next(updated_ratings);
            }
            ));
    }

    delete_rating() {
        this.view_ctrl.dismiss(true);
    }

    close() {
        this.view_ctrl.dismiss();
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad InstructorRatingPage");
    }
}
