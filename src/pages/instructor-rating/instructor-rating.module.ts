import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StarRatingModule } from "ionic3-star-rating";

import { PipesModule } from "../../pipes/pipes.module";

import { InstructorRatingPage } from "./instructor-rating";

@NgModule({
    declarations: [
        InstructorRatingPage,
    ],
    imports: [
        IonicPageModule.forChild(InstructorRatingPage),
        StarRatingModule,
        PipesModule,
    ],
})
export class InstructorRatingPageModule { }
