import { Component } from "@angular/core";
import {
    IonicPage, Loading, LoadingController, NavController,
    NavParams,
} from "ionic-angular";
import _ from "lodash";
import { tap } from "rxjs/operators";

import { Cdl } from "../../model/cdl/cdl.model";
import { Instructor } from "../../model/instructor/instructor.model";
import { CdlProvider } from "../../providers/cdl/cdl.provider";
import {
    InstructorProvider
} from "../../providers/instructor/instructor.provider";
// import { InstructorPage } from "../instructor/instructor";

@IonicPage()
@Component({
    selector: "page-instructor-search",
    templateUrl: "instructor-search.html",
})
export class InstructorSearchPage {

    private loading: Loading;
    private cdl_list: Cdl[] = [];
    private instructor_list: Instructor[] = [];
    private searchable_list: Instructor[] = [];
    private selected_cdl: Cdl;

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public loading_ctrl: LoadingController,
        public cdl_provider: CdlProvider,
        public instructor_provider: InstructorProvider) {
    }

    ngOnInit() {
        this.show_loading();
        this.cdl_provider.get_all_cdls()
            .pipe(
                tap((_cdl_list) => {
                    this.cdl_list = _cdl_list;
                    this.loading.dismiss();
                }),
        ).subscribe();
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad InstructorSearchPage");
    }

    selected_cdl_changed(new_cdl_id) {
        this.instructor_provider
            .get_instructors_from_cdl(new_cdl_id).subscribe(
                (_instructors) => {
                    this.instructor_list = _instructors;
                    this.instructor_list.sort(
                        (a, b) => a.name.localeCompare(b.name));
                    this.instructor_list = _.sortedUniqBy(
                        this.instructor_list, "id");
                    this.searchable_list = _
                        .cloneDeep(this.instructor_list);
                }
            );
    }

    search_items(str_to_search) {
        this.searchable_list = _.cloneDeep(this.instructor_list);
        if (str_to_search && str_to_search.trim() !== "") {
            this.searchable_list = this.searchable_list
                .filter((instructor) => (instructor.name.toLowerCase()
                    .indexOf(str_to_search.toLowerCase()) > -1));
        }
    }

    open_instructor(index) {
        let id = this.searchable_list[index].id;
        this.nav_ctrl.push("InstructorPage", { id: id });
    }
}
