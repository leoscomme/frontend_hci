import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";

import { InstructorSearchPage } from "./instructor-search";

@NgModule({
  declarations: [
    InstructorSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(InstructorSearchPage),
  ],
})
export class InstructorSearchPageModule {}
