import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StarRatingModule } from "ionic3-star-rating";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { CoursePage } from "./course";

@NgModule({
    declarations: [
        CoursePage,
    ],
    imports: [
        IonicPageModule.forChild(CoursePage),
        StarRatingModule,
        ComponentsModule,
        PipesModule,
    ],
})
export class CoursePageModule { }
