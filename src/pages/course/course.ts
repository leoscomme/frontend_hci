import {
    animate, state,
    style, transition, trigger
} from "@angular/animations";
import { Component } from "@angular/core";
import { ModalController, NavController, NavParams } from "ionic-angular";
import {
    AlertController,
    IonicPage,
    Loading,
    LoadingController,
    ToastController
} from "ionic-angular";
import _ from "lodash";
import { BehaviorSubject } from "rxjs";
import { map, tap } from "rxjs/operators";

import { Pds } from "../../model/pds/pds.model";
import { CourseRating } from "../../model/rating/rating.model";
import { CourseProvider } from "../../providers/course/course.provider";
import { PdsDataProvider } from "../../providers/data/pds-data.provider";
import { RatingDataProvider } from "../../providers/data/rating-data.provider";
// import { CourseRatingPage } from "../course-rating/course-rating";
// import { InstructorPage } from "../instructor/instructor";

@IonicPage()
@Component({
    selector: "course-page",
    templateUrl: "course.html",
    animations: [
        trigger("enterLeave", [
            transition(":enter", [
                style({ opacity: 0 }),
                animate(250, style({ opacity: 1 }))
            ]),
            transition(":leave", [
                style({ opacity: 1 }),
                animate(200, style({ opacity: 0 }))
            ])
        ]),
        trigger("arrow_rotation", [
            state("open", style({ transform: "rotate(-180deg)" })),
            state("closed", style({ transform: "rotate(0)" })),
            transition("open => closed",
                animate("250ms cubic-bezier(0.4, 0.0, 0.2, 1)")),
            transition("closed => open",
                animate("200ms cubic-bezier(0.4, 0.0, 0.2, 1)"))
        ]),
        trigger("openClose", [
            state("open", style({
                height: "*",
            })),
            state("closed", style({
                height: "0px",
            })),
            transition("open => closed", [
                animate("250ms cubic-bezier(0.4, 0.0, 0.2, 1)")
            ]),
            transition("closed => open", [
                animate("200ms cubic-bezier(0.4, 0.0, 0.2, 1)")
            ]),
        ]),
    ]

})
export class CoursePage {
    loading: Loading;
    private ratings: CourseRating[];
    private limit: number = 80;
    private truncating: boolean[];
    private user_rating: CourseRating | undefined;
    private expanded_item = {
        program: false,
        objectives_and_content: false
    };
    private selected_pds: Pds;
    private is_double: boolean;
    private is_in_pds: boolean;
    private version_idx: number;

    private selected_course = new BehaviorSubject(undefined);
    private ratings_source = new BehaviorSubject(undefined);

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public modal_ctrl: ModalController,
        public pds_provider: PdsDataProvider,
        public rating_provider: RatingDataProvider,
        public course_provider: CourseProvider,
        public alert_ctrl: AlertController,
        public loading_ctrl: LoadingController,
        private toast_ctrl: ToastController) {
    }

    ngOnInit() {
        this.show_loading();
        let first = this.nav_params.get("first");
        let second = this.nav_params.get("second");
        let id: number;
        if (second !== undefined) {
            id = first.id > second.id ? first.id : second.id;
            // this.is_double = true;
        } else {
            id = first.id;
        }
        if (id !== first.id) {
            let tmp = first;
            first = second;
            second = tmp;
        }
        this.get_course(id, first, second).subscribe(() => {
            // FIXME inner subscribe da evitare
            this.rating_provider.get_rating_from_course_title(
                this.selected_course.value.title).subscribe((_rating) => {
                    this.user_rating = _rating;
                    console.log("user_rating", this.user_rating);
                    this.get_selected_pds();
                });

        });
        this.get_ratings_from_course_id(id).subscribe(() => {
            this.loading.dismiss();
        });
        // this.get_ratings_from_course_id(id).subscribe();
    }

    expand(item) {
        this.expanded_item[item] = !this.expanded_item[item];
    }

    get_selected_pds() {
        this.pds_provider.get_current_pds().subscribe(
            (_pds) => {
                this.selected_pds = _pds;

                let idx = _.findIndex(this.selected_pds.courses,
                    { title: this.selected_course.value.title });

                this.is_double = this.selected_course
                    .value.available_cfus.length > 1 ? true : false;
                // The course is already in pds, check wich version.
                if (idx >= 0) {
                    this.is_in_pds = true;
                    let found_course = this.selected_pds.courses[idx];
                    let selected_course = this.selected_course.value;

                    // The index of the cfu version of the present course in
                    // the pds.
                    if (selected_course.available_cfus.length > 1) {
                        this.version_idx = selected_course
                            .available_cfus.indexOf(found_course.cfu);
                    }
                }
                // The course is not in the pds
                else {
                    this.is_in_pds = false;
                }
            }
        );
    }

    print_cfus(available_cfus) {
        let cfus_str: string;
        if (available_cfus.length === 2) {
            let smaller = Math.min(available_cfus[0], available_cfus[1]);
            let bigger = Math.max(available_cfus[0], available_cfus[1]);
            cfus_str = smaller + ", " + bigger;
        } else {
            cfus_str = available_cfus[0];
        }
        return cfus_str;
    }

    get_course(id: number, first, second) {
        return this.course_provider.get_course(id).pipe(map(
            (_course) => {
                _course.available_ids = [first.id];
                _course.available_cfus = [first.cfu];
                _course.available_codes = [first.code];
                if (second !== undefined) {
                    _course.available_ids.push(second.id);
                    _course.available_cfus.push(second.cfu);
                    _course.available_codes.push(second.code);
                }
                this.selected_course.next(_course);
            }
        ));
    }

    get_ratings_from_course_id(id: number) {
        return this.course_provider.get_ratings_from_course_id(id).pipe(map(
            (_ratings) => {
                this.ratings_source.next(_ratings);

                this.ratings_source.subscribe((_sbj_ratings) => {
                    this.truncating = new Array<boolean>(_sbj_ratings.length);
                    for (let i in _sbj_ratings) {
                        this.truncating[i] =
                            _sbj_ratings[i].comment.length >
                                this.limit ? true : false;
                    }
                });
            }
        ));
    }

    convert_date(date) {
        date = new Date(date);
        return date.toLocaleString("it-IT").split(":").slice(0, -1).join(":");
    }

    leave_new_rating() {
        let rate_page = this.modal_ctrl.create("CourseRatingPage",
            {
                course: this.selected_course,
                rate_sbj: this.ratings_source,
            });
        rate_page.present();
    }

    edit_rating() {
        let params = {
            course: this.selected_course,
            rate_sbj: this.ratings_source,
            rating: undefined
        };
        if (this.user_rating) {
            params.rating = this.user_rating;
        }
        let rate_page = this.modal_ctrl.create("CourseRatingPage",
            params);
        rate_page.onDidDismiss((remove) => {
            if (remove) {
                this.delete_rating();
            }
        });
        rate_page.present();
    }

    delete_rating(): any {
        const confirm = this.alert_ctrl.create({
            title: "Attenzione",
            message: "Vuoi davvero eliminare il commento?",
            buttons: [
                {
                    text: "Annulla",
                    handler: () => {
                        console.log("Disagree clicked");
                    }
                },
                {
                    text: "Elimina",
                    handler: () => {
                        // update stars
                        let current_ratings = this.ratings_source.value;
                        let ratings_num = current_ratings.length;
                        let new_mean;
                        if (ratings_num === 1) {
                            new_mean = 0;
                        } else {
                            new_mean = (
                                (this.selected_course.value.stars * ratings_num)
                                - this.user_rating.stars) / (ratings_num - 1);
                        }
                        let new_course = this.selected_course.value;
                        new_course.stars = new_mean;

                        return this.course_provider
                            .delete_course_rating(this.user_rating.id)
                            .pipe(tap(() => {
                                this.selected_course.next(new_course);
                                let ratings = this.ratings_source.value;
                                const index: number = ratings
                                    .indexOf(this.user_rating);
                                ratings.splice(index, 1);
                                this.ratings_source.next(ratings);
                                // elimino l'unico rating
                                this.rating_provider
                                    .delete_course_rating(this.user_rating);
                            })).subscribe();
                    }
                }
            ]
        });
        confirm.present();
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ValutazioneEsamePage");
    }

    open_instructor(id) {
        this.nav_ctrl.push("InstructorPage", { id: id });
    }

    update_pds() {
        // console.log("HomePage::update_pds");
        this.pds_provider.update_pds(this.selected_pds);
    }

    present_toast(message) {
        let toast = this.toast_ctrl.create({
            message: message,
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    }

    add_course_or_change_version() {
        let course = this.selected_course.value;

        if (course.academic_year !== "2017/2018") {
            const alert = this.alert_ctrl.create({
                title: "Attenzione",
                message: "Impossibile aggiungere un corso"
                    + " di un diverso anno accademico.",
                buttons: [{ text: "Ok" }]
            });
            alert.present();
            return;
        }

        // Case 1) Single course.
        if (!this.is_double) {
            this.present_toast(
                `Aggiunto ${course.title}`);
            course.cfu = course.available_cfus[0];
            course.code = course.available_codes[0];
            course.id = course.available_ids[0];
            this.selected_pds.courses.push(course);
        }

        // Case 2) Double course.
        else {
            // Case 2.1) This means the user has chosen to change
            // the version of the double course already in pds.
            if (this.is_in_pds) {
                let new_version_idx = this.version_idx === 0 ? 1 : 0;
                this.version_idx = new_version_idx;
                let new_cfu = course.available_cfus[new_version_idx];
                this.present_toast(
                    `${course.cfu} => ${new_cfu}: ${course.title}`);

                course.cfu = new_cfu;

                let idx = _.findIndex(this.selected_pds.courses,
                    { title: this.selected_course.value.title });
                // Replace the other version with the current one.
                this.selected_pds.courses[idx]
                    .cfu = course.available_cfus[new_version_idx];
                this.selected_pds.courses[idx]
                    .code = course.available_codes[new_version_idx];
                this.selected_pds.courses[idx]
                    .id = course.available_ids[new_version_idx];

            }
            // Case 2.2) The user has chosen to add a new double course in pds
            else {
                let alert = this.alert_ctrl.create();
                alert.setTitle("Versione");

                for (let i in this.selected_course.value.available_cfus) {
                    let curr_cfu = this.selected_course.value.available_cfus[i];
                    alert.addInput({
                        type: "radio",
                        label: curr_cfu + " CFU",
                        value: i,
                        checked: false
                    });
                }

                alert.addButton("Cancella");
                alert.addButton({
                    text: "Aggiungi",
                    handler: (index) => {
                        console.log(index);
                        this.present_toast(
                            `Aggiunto: ${course.title}`);
                        this.version_idx = index;
                        course.cfu = course.available_cfus[this.version_idx];
                        course.code = course.available_codes[this.version_idx];
                        course.id = course.available_ids[this.version_idx];
                        // Just push the newly added course.
                        this.selected_pds.courses.push(course);
                        this.update_pds();
                    }
                });
                alert.present();
            }
        }
        this.update_pds();
    }

    remove_course() {
        let course = this.selected_course.value;
        this.present_toast(
            `Rimosso: ${course.title}`);
        let pds_index = _.findIndex(this.selected_pds.courses,
            { title: course.title });
        this.selected_pds.courses.splice(pds_index, 1);
        course.cfu = undefined;
        course.code = undefined;
        this.update_pds();
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }
    // add_or_remove_course() {
    //     // The course is already in the pds => remove.
    //     if (this.is_in_pds) {
    //         this._remove_course();
    //     }
    //     // The course is not in the pds => add it / change version.
    //     else {
    //         this._add_course();
    //     }
    // }
}
