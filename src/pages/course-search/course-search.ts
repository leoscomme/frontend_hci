import { Component } from "@angular/core";
import {
    IonicPage, Loading, LoadingController, NavController,
    NavParams,
} from "ionic-angular";
import _ from "lodash";
import { tap } from "rxjs/operators";

import { Cdl } from "../../model/cdl/cdl.model";
import { Course } from "../../model/course/course.model";
import { CdlProvider } from "../../providers/cdl/cdl.provider";
// import { CoursePage } from "../course/course";

@IonicPage()
@Component({
    selector: "page-course-search",
    templateUrl: "course-search.html",
})
export class CourseSearchPage {

    private loading: Loading;
    private cdl_list: Cdl[] = [];
    private course_list: Course[] = [];
    private searchable_list: Course[] = [];
    private selected_cdl: Cdl;

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public loading_ctrl: LoadingController,
        public cdl_provider: CdlProvider) {
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

    ngOnInit() {
        this.show_loading();
        this.cdl_provider.get_all_cdls()
            .pipe(
                tap((_cdl_list) => {
                    this.cdl_list = _cdl_list;
                    this.loading.dismiss();
                }),
        ).subscribe();
    }

    search_items(str_to_search) {
        this.searchable_list = _.cloneDeep(this.course_list);
        // if the value is an empty string don't filter the items
        if (str_to_search && str_to_search.trim() !== "") {
            this.searchable_list = this.searchable_list
                .filter((course) => (course.title.toLowerCase()
                    .indexOf(str_to_search.toLowerCase()) > -1));
        }
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad CourseSearchPage");
    }

    selected_cdl_changed(new_cdl_id) {
        this.cdl_provider.get_cdl(new_cdl_id, "2017/2018")
            .pipe(
                tap((_cdl) => {
                    this.course_list = _cdl.courses;
                    this.searchable_list = _.cloneDeep(this.course_list);
                }),
        ).subscribe();
    }

    open_course(index) {
        let id: number;
        let idx: number = 0;
        let sec_idx: number;
        let params = { first: undefined, second: undefined };

        let course_to_open = this.searchable_list[index];
        console.log(course_to_open);

        if (course_to_open.available_cfus.length === 2) {
            sec_idx = 1;
        }

        id = course_to_open.available_ids[idx];
        let cfu = course_to_open.available_cfus[idx];
        let code = course_to_open.available_codes[idx];
        params.first = { id: id, cfu: cfu, code: code };

        if (sec_idx !== undefined) {
            let sec_id = course_to_open.available_ids[sec_idx];
            let sec_cfu = course_to_open.available_cfus[sec_idx];
            let sec_code = course_to_open.available_codes[sec_idx];
            params.second = { id: sec_id, cfu: sec_cfu, code: sec_code };
        }

        this.nav_ctrl.push("CoursePage", params);
    }

}
