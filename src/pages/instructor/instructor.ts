import { Component } from "@angular/core";
import { AlertController, IonicPage } from "ionic-angular";
import { Loading, LoadingController,
    ModalController, NavController, NavParams } from "ionic-angular";
import _ from "lodash";
import { BehaviorSubject } from "rxjs";
import { map, tap } from "rxjs/operators";

import { Course } from "../../model/course/course.model";
import {
    RatingDataProvider
} from "../../providers/data/rating-data.provider";
import {
    InstructorProvider
} from "../../providers/instructor/instructor.provider";
// import { CoursePage } from "../course/course";
// import {
//     InstructorRatingPage
// } from "../instructor-rating/instructor-rating";

@IonicPage()
@Component({
    selector: "instructor-page",
    templateUrl: "instructor.html",
})
export class InstructorPage {
    loading: Loading;
    private limit: number = 80;
    private truncating: boolean[];

    private selected_instructor = new BehaviorSubject(undefined);
    private ratings_source = new BehaviorSubject(undefined);
    private user_rating: any | undefined;

    constructor(public nav_ctrl: NavController, public nav_params: NavParams,
    public modal_ctrl: ModalController,
    public rating_provider: RatingDataProvider,
    public instructorProvider: InstructorProvider,
    public loading_ctrl: LoadingController,
    public alert_ctrl: AlertController) {
    }

    ngOnInit() {
    this.show_loading();
        let id = this.nav_params.get("id");  // this is taken from app's url
        this.get_instructor(id).subscribe(() => {
            this.rating_provider.get_rating_from_instructor_title(
                this.selected_instructor.value.name).subscribe((_rating) => {
                    console.log("rating in instructor page init ", _rating);
                    this.user_rating = _rating;
                });
        });
        this.get_ratings_from_instructor_id(id).subscribe(()=>{
        this.loading.dismiss();
    });
// 	this.get_ratings_from_instructor_id(id).subscribe();
    }

    get_instructor(id: number) {
        return this.instructorProvider.get_instructor(id).pipe(map(
            (_instructor) => {
                /* ----------------------- */
                let transformed_courses: Course[] = [];  // result

                // Iterate the response and merge courses
                for (let current_course of _instructor.courses) {

                    // let current_course = _instructor.courses[i];
                    let curr_index = _.findIndex(transformed_courses,
                        ["title", current_course.title]);

                    if (curr_index < 0) {
                        let available_cfus = [current_course.cfu];
                        let available_codes = [current_course.code];
                        let available_ids = [current_course.id];
                        current_course.available_cfus = available_cfus;
                        current_course.available_codes = available_codes;
                        current_course.available_ids = available_ids;
                        current_course.cfu = undefined;
                        current_course.code = undefined;
                        current_course.id = undefined;
                        transformed_courses.push(current_course);
                    } else {
                        transformed_courses[curr_index].
                            available_cfus.push(current_course.cfu);
                        transformed_courses[curr_index].
                            available_codes.push(current_course.code);
                        transformed_courses[curr_index].
                            available_ids.push(current_course.id);
                    }

                }
                // console.log(transformed_courses);
                _instructor.courses = transformed_courses;
                /* ----------------------- */
                this.selected_instructor.next(_instructor);
            }
        ));
    }

    convert_date(date) {
        date = new Date(date);
        return date.toLocaleString("it-IT").split(":").slice(0, -1).join(":");
    }

    get_ratings_from_instructor_id(id: number) {
        return this.instructorProvider
            .get_ratings_from_instructor_id(id)
            .pipe(map(
                (_ratings) => {
                    this.ratings_source.next(_ratings);

                    this.ratings_source.subscribe((_sbj_ratings) => {
                        this.truncating = new Array<boolean>(
                            _sbj_ratings.length);
                        for (let i in _sbj_ratings) {
                            this.truncating[i] =
                                _sbj_ratings[i].comment.length >
                                    this.limit ? true : false;
                        }
                    });

                }
            ));
    }

    leave_new_rating() {
        let rate_page = this.modal_ctrl.create("InstructorRatingPage",
            {
                instructor: this.selected_instructor,
                rate_sbj: this.ratings_source,
            });
        rate_page.present();
    }

    edit_rating() {
        let rate_page = this.modal_ctrl.create("InstructorRatingPage",
            {
                instructor: this.selected_instructor,
                rate_sbj: this.ratings_source,
                rating: this.user_rating
            });
        rate_page.onDidDismiss((remove) => {
            if (remove) {
                this.delete_rating();
            }
        });
        rate_page.present();
    }

    delete_rating(): any {
        const confirm = this.alert_ctrl.create({
            title: "Attenzione",
            message: "Vuoi davvero eliminare il commento?",
            buttons: [
                {
                    text: "Annulla",
                    handler: () => {
                        console.log("Disagree clicked");
                    }
                },
                {
                    text: "Elimina",
                    handler: () => {
                        // update stars
                        let current_ratings = this.ratings_source.value;
                        let ratings_num = current_ratings.length;
                        let new_mean;
                        if (ratings_num === 1) {
                            new_mean = 0;
                        } else {
			    new_mean =
				((this.selected_instructor
				  .value.stars * ratings_num)
				 - this.user_rating.stars)
				/ (ratings_num - 1);
                        }

                        let new_instructor = this.selected_instructor.value;
                        new_instructor.stars = new_mean;

                        return this.instructorProvider
                            .delete_instructor_rating(this.user_rating.id)
                            .pipe(tap(() => {
                                this.selected_instructor.next(new_instructor);
                                let ratings = this.ratings_source.value;
                                const index: number =
                                    ratings.indexOf(this.user_rating);
                                ratings.splice(index, 1);
                                this.ratings_source.next(ratings);
                                this.rating_provider
                                    .delete_instructor_rating(this.user_rating);
                            })).subscribe();
                    }
                }
            ]
        });
        confirm.present();
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad InstructorPage");
    }

    open_course(index) {

        let params = { first: undefined, second: undefined };
        let course = this.selected_instructor.value.courses[index];

        // params.id = course.available_cfus[0];
        let id = course.available_ids[0];
        let code = course.available_codes[0];
        let cfu = course.available_cfus[0];
        params.first = { id: id, cfu: cfu, code: code };
        if (course.available_cfus.length === 2) {
            params.second = {
                id: course.available_ids[1],
                cfu: course.available_cfus[1],
                code: course.available_codes[1]
            };
        }

        this.nav_ctrl.push("CoursePage", params);
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

}
