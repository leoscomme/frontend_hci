import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StarRatingModule } from "ionic3-star-rating";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { InstructorPage } from "./instructor";

@NgModule({
    declarations: [
        InstructorPage,
    ],
    imports: [
        IonicPageModule.forChild(InstructorPage),
        StarRatingModule,
        ComponentsModule,
        PipesModule
    ],
})
export class InstructorPageModule { }
