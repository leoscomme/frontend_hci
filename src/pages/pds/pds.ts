import { animate, style, transition, trigger } from "@angular/animations";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AlertController } from "ionic-angular";
import _ from "lodash";

import { Pds } from "../../model/pds/pds.model";
import { PdsDataProvider } from "../../providers/data/pds-data.provider";
// import { HomePage } from "../home/home";

@IonicPage()
@Component({
    selector: "page-pds",
    templateUrl: "pds.html",

    animations: [
        trigger("enterLeave", [
            transition(":enter", [
                style({ opacity: 0 }),
                animate(250, style({ opacity: 1 }))
            ]),
            transition(":leave", [
                style({ opacity: 1 }),
                animate(200, style({ opacity: 0 }))
            ])
        ])
    ]

})
export class PdsPage {

    private selected_pds: Pds;
    private user_pds_list: Pds[];
    private expand_pds: boolean[];
    private currently_expanded: number;

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public pds_provider: PdsDataProvider,
        public alert_ctrl: AlertController) {
    }

    ngOnInit() {
        console.log("ngOnInit PdsPage");
        this.get_pds_list();
        this.get_selected_pds();
    }

    ionViewDidLoad() {
        // console.log('ionViewDidLoad PdsPage');
    }

    get_selected_pds() {
        this.pds_provider.get_current_pds().subscribe(
            (_pds) => {
                this.selected_pds = _pds;
                // console.log("get_selected_pds -> assigned _pds");
            }
        );
    }

    get_pds_list() {
        // console.log("get_pds_list");
        this.pds_provider.get_pds_list().subscribe(
            (_pds_list) => {
                console.log("PdsPage: updating pds_list");
                this.user_pds_list = _pds_list;
                // console.log("get_pds_list -> assigned _pds_list");
                // console.log("user_pds_list: ", this.user_pds_list);
                this.expand_pds = new Array<boolean>(
                    this.user_pds_list.length);
                for (let i = 0; i < this.user_pds_list.length; i++) {
                    this.expand_pds[i] = false;
                }
            }
        );
    }

    expand_item(index) {
        if (this.currently_expanded === index) {
            this.expand_pds[index] = !this.expand_pds[index];
            return;
        }
        this.currently_expanded = index;
        for (let i in this.expand_pds) {
            this.expand_pds[i] = false;
        }
        this.expand_pds[index] = true;
    }

    select_pds(index) {
        console.log("Select clicked");
        let curr_index = _.findIndex(this.user_pds_list,
            ["id", this.selected_pds.id]);
        console.log(curr_index, index);
        if (curr_index !== index) {
            this.pds_provider.set_current_pds(this.user_pds_list[index]);
            this.navigate_to_home_page();
        }
    }

    create_empty_pds(pds_name: string) {
        console.log("create_empty_pds:: pds_name: " + pds_name);
        let new_pds = new Pds({ name: pds_name });
        console.log("create_empty_pds:: new_pds: " + new_pds);
        this.pds_provider.save_pds(new_pds);
        // localStorage.setItem("at_least_one_pds", "true");
        this.pds_provider.pds_provider.at_least_one_pds.next(true);
        this.navigate_to_home_page();
    }

    show_prompt() {
        const prompt = this.alert_ctrl.create({
            title: "Crea nuovo PdS",
            message: "Inserisci il nome del nuovo Piano di Studi.",
            inputs: [
                {
                    name: "pds_name",
                    placeholder: "Nome"
                },
            ],
            buttons: [
                {
                    text: "Annulla",
                    handler: () => {
                        console.log("Annulla clicked");
                    }
                },
                {
                    text: "Crea",
                    handler: (data) => {
                        console.log("Crea clicked");
                        this.create_empty_pds(data["pds_name"]);
                    }
                }
            ]
        });
        prompt.present();
    }

    navigate_to_home_page() {
        this.nav_ctrl.setRoot("HomePage", {},
            {
                animate: true,
                animation: "md-transition",
                direction: "forward",
                duration: 250,
                easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
            });
    }

    rename_pds() {
        const prompt = this.alert_ctrl.create({
            title: "Rinomina",
            message: "Inserisci il nuovo nome per il Piano di Studi",
            inputs: [
                {
                    name: "name",
                    // placeholder: this.selected_pds.name,
                    value: this.selected_pds.name
                },
            ],
            buttons: [
                {
                    text: "Annulla",
                    handler: () => {
                        console.log("Cancel clicked");
                    }
                },
                {
                    text: "Rinomina",
                    handler: (data) => {
                        this.selected_pds.name = data["name"];
                        this.update_pds();
                    }
                }
            ]
        });
        prompt.present();

    }
    
    rename_pds_index(index) {
	let pds = this.user_pds_list[index]	
        const prompt = this.alert_ctrl.create({
            title: "Rinomina",
            message: "Inserisci il nuovo nome per il Piano di Studi",
            inputs: [
                {
                    name: "name",
                    // placeholder: this.selected_pds.name,
                    value: pds.name
                },
            ],
            buttons: [
                {
                    text: "Annulla",
                    handler: () => {
                        console.log("Cancel clicked");
                    }
                },
                {
                    text: "Rinomina",
                    handler: (data) => {
			this.rename_pds_from_index(pds.id, data["name"]);
                    }
                }
            ]
        });
        prompt.present();

    }
    

    rename_pds_from_index(id: number, name: string){
	this.pds_provider.rename_pds_from_id(id, name);
    }
    
    update_pds() {
        // console.log("HomePage::update_pds");
        this.pds_provider.update_pds(this.selected_pds);
    }
    
    delete_pds() {
        const confirm = this.alert_ctrl.create({
	    title: "Attenzione",
	    message: "Vuoi davvero eliminare il PdS "
                + this.selected_pds.name + "?",
	    buttons: [
                {
		    text: "Annulla",
		    handler: () => {
                        console.log("Disagree clicked");
		    }
                },
                {
		    text: "Elimina",
		    handler: () => {
                        this.pds_provider.delete_pds(this.selected_pds.id);
		    }
                }
	    ]
        });
        confirm.present();
    }

    delete_pds_index(index) {
	let pds = this.user_pds_list[index]
	const confirm = this.alert_ctrl.create({
	    title: "Attenzione",
	    message: "Vuoi davvero eliminare il PdS "
                + pds.name + "?",
	    buttons: [
                {
                    text: "Annulla",
                    handler: () => {
                        console.log("Disagree clicked");
                    }
                },
                {
                    text: "Elimina",
                    handler: () => {
                        this.pds_provider.delete_pds(pds.id);
                    }
                }
            ]
        });
        confirm.present();
    }

}
