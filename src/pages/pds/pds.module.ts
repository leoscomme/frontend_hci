import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";

import { ComponentsModule } from "../../components/components.module";

import { PdsPage } from "./pds";

@NgModule({
    declarations: [
        PdsPage,
    ],
    imports: [
        IonicPageModule.forChild(PdsPage),
        ComponentsModule
    ],
})
export class PdsPageModule { }
