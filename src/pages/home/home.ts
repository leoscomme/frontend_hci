import {
    animate,
    state,
    style,
    transition,
    trigger,
} from "@angular/animations";
import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { AlertController } from "ionic-angular";
import { ToastController } from "ionic-angular";
import {
    IonicPage,
    Loading,
    LoadingController
} from "ionic-angular";
import _ from "lodash";
import { Observable } from "rxjs";

import { Cdl } from "../../model/cdl/cdl.model";
import { Course } from "../../model/course/course.model";
import { Pds } from "../../model/pds/pds.model";
import { CdlDataProvider } from "../../providers/data/cdl-data.provider";
import { PdsDataProvider } from "../../providers/data/pds-data.provider";
import { PdsProvider } from "../../providers/pds/pds.provider";
// import { CourseSearchPage } from "../course-search/course-search";
// import { CoursePage } from "../course/course";
// import { InstructorSearchPage
// } from "../instructor-search/instructor-search";
// import { PdsPage } from "../pds/pds";

@IonicPage()
@Component({
    selector: "page-home",
    templateUrl: "home.html",
    animations: [
        trigger("arrow_rotation", [
            state("open", style({ transform: "rotate(-180deg)" })),
            state("closed", style({ transform: "rotate(0)" })),
            transition("open => closed",
                animate("250ms cubic-bezier(0.4, 0.0, 0.2, 1)")),
            transition("closed => open",
                animate("200ms cubic-bezier(0.4, 0.0, 0.2, 1)"))
        ]),
    ],
})
export class HomePage {

    loading: Loading;
    selected_cdl: Cdl;
    selected_pds: Pds;
    merged_courses: Course[];
    expand_course: boolean[];
    subscription: any;
    init_count: number = 0;
    show_welcome: boolean = false;

    private app_name = "UniFiAdvisor";

    constructor(
        public nav_ctrl: NavController,
        public cdl_provider: CdlDataProvider,
        public pds_data_provider: PdsDataProvider,
        public pds_provider: PdsProvider,
        public alert_ctrl: AlertController,
        public loading_ctrl: LoadingController,
        private toast_ctrl: ToastController) {
    }

    ngOnInit() {
        console.log("ngOnInit");

        this.show_loading();

        this.pds_provider.at_least_one_pds
            .filter((val) => {
                console.log("val", val);
                return val !== null;
            })
            .subscribe(
                (at_least_one_pds) => {
                    console.log("at_least_one_pds", at_least_one_pds);
                    if (at_least_one_pds) {
                        this.show_welcome = false;
                    }
                    else {
                        this.show_welcome = true;
                    }
                    this.loading.dismiss();
                    const combined = Observable.combineLatest(
                        this.cdl_provider
                            .get_current_cdl().distinctUntilChanged(),
                        this.pds_data_provider
                            .get_current_pds().distinctUntilChanged()
                    );

                    this.subscription = combined.subscribe((arr) => {
                        console.log("**** combined.subscribe ****");
                        let _cdl = arr[0] as Cdl;
                        let _pds = arr[1] as Pds;
                        if (this.selected_pds
                            && _pds.id !== this.selected_pds.id) {
                            this.init_count = 0;
                        }
                        this.selected_cdl = _cdl;
                        this.selected_pds = _pds;
                        let old_merged_courses = _
                            .cloneDeep(this.merged_courses);
                        this.merged_courses = _
                            .cloneDeep(this.selected_cdl.courses);
                        if (old_merged_courses) {
                            for (let i in this.merged_courses) {
                                this.merged_courses[i]["visible"] =
                                    old_merged_courses[i]["visible"];
                            }
                        }
                        this.separate_pds_cdl_courses();
                        if (this.init_count === 0) {
                            this.initialize_visibility();
                            this.close_all();
                            this.init_count++;
                        }

                    });
                }
            );
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

    ionViewCanLeave() {
        console.log("ionViewCanLeave HomePage");
        return true;
    }

    close_all() {
        this.expand_course = new Array<boolean>(
            this.merged_courses.length);
        _.fill(this.expand_course, false);
    }

    open_course(index, version_idx?) {
        // console.log("open_course: -------------------");
        let id: number;
        let idx: number;
        let sec_idx: number;
        let params = { first: undefined, second: undefined };

        let course_to_open = this.merged_courses[index];
        // console.log(course_to_open);

        if (course_to_open.available_cfus.length === 1) {
            idx = 0;
        } else {
            if (course_to_open.id !== undefined) {
                idx = _.indexOf(course_to_open.available_ids,
                    course_to_open.id);
            } else {
                idx = version_idx;
            }
            sec_idx = (idx + 1) % 2;
        }

        id = course_to_open.available_ids[idx];
        let cfu = course_to_open.available_cfus[idx];
        let code = course_to_open.available_codes[idx];
        params.first = { id: id, cfu: cfu, code: code };
        if (sec_idx !== undefined) {
            let sec_id = course_to_open.available_ids[sec_idx];
            let sec_cfu = course_to_open.available_cfus[sec_idx];
            let sec_code = course_to_open.available_codes[sec_idx];
            params.second = { id: sec_id, cfu: sec_cfu, code: sec_code };
        }

        this.nav_ctrl.push("CoursePage", params);
    }

    separate_pds_cdl_courses() {
        // Find index in merged_courses for every course in pds.
        // Set cfu and code in merged_courses.
        for (let course of this.selected_pds.courses) {
            let index = _.findIndex(this.merged_courses,
                { title: course.title });
            let m_course = this.merged_courses[index];
            m_course.cfu = course.cfu;
            m_course.code = course.code;
            m_course.id = course.id;
        }

        /* Now if cfu and code are undefined, it means that the course is not
           in the current pds. Else if cfu and code are not undefined, they
           represent the course version selected in the pds. */
    }

    rename_pds() {
        const prompt = this.alert_ctrl.create({
            title: "Rinomina",
            message: "Inserisci il nuovo nome per il Piano di Studi",
            inputs: [
                {
                    name: "name",
                    // placeholder: this.selected_pds.name,
                    value: this.selected_pds.name
                },
            ],
            buttons: [
                {
                    text: "Annulla",
                    handler: () => {
                        console.log("Cancel clicked");
                    }
                },
                {
                    text: "Rinomina",
                    handler: (data) => {
                        this.selected_pds.name = data["name"];
                        this.update_pds();
                    }
                }
            ]
        });
        prompt.present();

    }

    initialize_visibility() {
        for (let m_course of this.merged_courses) {
            m_course["visible"] = true;
        }
    }

    search_items(str_to_search) {
        this.initialize_visibility();

        // if the value is an empty string don't filter the items
        if (str_to_search && str_to_search.trim() !== "") {
            str_to_search = str_to_search.toLowerCase();
            for (let m_course of this.merged_courses) {
                if (m_course.title.toLowerCase().indexOf(str_to_search) > -1) {
                    m_course["visible"] = true;
                }
                else {
                    m_course["visible"] = false;
                }

            }
        }
    }

    save_pds() {
        this.pds_data_provider.save_pds(this.selected_pds);
    }

    update_pds() {
        // console.log("HomePage::update_pds");
        this.pds_data_provider.update_pds(this.selected_pds);
    }

    present_toast(message) {
        let toast = this.toast_ctrl.create({
            message: message,
            duration: 2000,
            position: "bottom"
        });
        toast.present();
    }

    add_course(index, _cfu?) {
        // console.log("add course");
        let m_course = this.merged_courses[index];

        // Case 1) _cfu was not passed and is undefined.
        // This means that the method was called from a single course.
        if (_cfu === undefined) {
            this.present_toast(
                `Aggiunto ${m_course.title}`);
            m_course.cfu = m_course.available_cfus[0];
            m_course.code = m_course.available_codes[0];
            m_course.id = m_course.available_ids[0];
            this.selected_pds.courses.push(m_course);
            // this.update_pds();
        } else {
            // Try to find the double course in the pds
            let idx = _.findIndex(this.selected_pds.courses,
                { title: m_course.title });
            // console.log("add_course::idx", idx);
            // console.log("add_course::course_to_add:");
            // console.log(m_course);

            // If the double course is already in the pds, it means the user
            // has chosen to change the version of the course.
            // Present an alert to inform the user.
            if (idx >= 0) {
                this.present_toast(
                    `${m_course.cfu} => ${_cfu}: ${m_course.title}`);

                let i = m_course.available_cfus[0] === _cfu ?
                    0 : 1;

                // Replace the other version with the current one.
                this.selected_pds.courses[idx]
                    .cfu = m_course.available_cfus[i];
                this.selected_pds.courses[idx]
                    .code = m_course.available_codes[i];
                this.selected_pds.courses[idx]
                    .id = m_course.available_ids[i];
                // this.update_pds();

            } else {
                this.present_toast(
                    `Aggiunto: ${m_course.title}`);
                let i = m_course.available_cfus[0] === _cfu ? 0 : 1;
                m_course.cfu = m_course.available_cfus[i];
                m_course.code = m_course.available_codes[i];
                m_course.id = m_course.available_ids[i];

                // Just push the newly added course.
                this.selected_pds.courses.push(m_course);
                // this.update_pds();
            }
        }
        this.update_pds();
    }

    remove_course(m_index) {
        let m_course = this.merged_courses[m_index];
        this.present_toast(
            `Rimosso: ${m_course.title}`);
        let pds_index = _.findIndex(this.selected_pds.courses,
            { title: m_course.title });
        this.selected_pds.courses.splice(pds_index, 1);
        m_course.cfu = undefined;
        m_course.code = undefined;
        this.update_pds();
    }

    expand_item(index) {
        if (this.expand_course[index] === true) {
            this.expand_course[index] = false;
            return;
        }
        _.fill(this.expand_course, false);
        this.expand_course[index] = true;
        // this.expand_course[index] = !this.expand_course[index];
    }

    navigate_to_pds_page() {
        this.nav_ctrl.setRoot("PdsPage", {},
            {
                animate: true,
                animation: "md-transition",
                direction: "forward",
                duration: 250,
                easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
            });
    }
    navigate_to_search_course_page() {
        this.nav_ctrl.setRoot("CourseSearchPage", {},
            {
                animate: true,
                animation: "md-transition",
                direction: "forward",
                duration: 250,
                easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
            });
    }
    navigate_to_search_instructor_page() {
        this.nav_ctrl.setRoot("InstructorSearchPage", {},
            {
                animate: true,
                animation: "md-transition",
                direction: "forward",
                duration: 250,
                easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
            });
    }

    hide_line(j, m_course_cfu) {
        let hide_line: boolean;

        if ((j === 0 && m_course_cfu !== undefined) || (j === 1)) {
            hide_line = true;
        }
        else {
            hide_line = null;
        }

        return hide_line;
    }
}
