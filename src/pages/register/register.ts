import { Component } from "@angular/core";
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from "@angular/forms";
import {
    IonicPage, Loading, LoadingController,
    NavController, NavParams
} from "ionic-angular";

import { AlertProvider } from "../../providers/alert/alert.provider";
import { AuthProvider } from "../../providers/auth/auth.provider";
// import { LoginPage } from "../login/login";

@IonicPage()
@Component({
    selector: "page-register",
    templateUrl: "register.html",
})
export class RegisterPage {

    private loading: Loading;
    private email_form: FormControl;
    private register_form: FormGroup;
    private passwords_form: FormGroup;

    private submit_attempt: boolean = false;
    private email_err_submit: boolean = false;
    private pass_err_submit: boolean = false;

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public form_builder: FormBuilder,
        private loading_ctrl: LoadingController,
        private auth_provider: AuthProvider,
        private alert: AlertProvider
    ) {

        this.register_form = form_builder.group({
            email: ["", Validators.compose(
                [Validators.required, Validators
                    .pattern("^[a-zA-Z0-9.]+@stud\.unifi\.it$")])
            ],
            passwords: this.form_builder.group(
                {
                    password: ["", Validators
                        .compose(
                            [Validators.required, Validators.minLength(8)]
                        )
                    ],
                    confirm_password: ["", Validators
                        .compose(
                            [Validators.required,
                            this.password_match_validator,
                            Validators.minLength(8)]
                        )]
                },
            ),
        });
    }

    password_match_validator(form_group: FormGroup) {
        if (!form_group.parent) {
            return null;
        }
        const controls = form_group.parent.controls;
        if (controls && controls["password"]
            && controls["confirm_password"]) {
            if (controls["password"].value
                !== controls["confirm_password"].value) {
                return { mismatch: true };
            }
        }
        return null;
    }

    registration() {
        if (this.register_form.valid) {
            this.submit_attempt = false;
            console.log("registration");

            // nuovo
            this.show_loading();

            let email = this.register_form.controls.email.value;
            let passwords = this.register_form.controls.passwords.value;

            // credentials da cambiare
            this.auth_provider.register(email, passwords)
                .subscribe((response) => {
                    if (response) {
                        this.loading.dismiss();
                        this.alert.show_stack("<br>Utente creato con successo",
                            "la tua mail è:<br>" + email
                            + "<br><br>Il tuo Username è:<br>"
                            + email.split("@")[0]);
                        this.nav_ctrl.setRoot("LoginPage", {},
                            {
                                animate: true,
                                animation: "md-transition",
                                direction: "forward",
                                duration: 250,
                                easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
                            });
                    } else {
                        this.loading.dismiss();
                        this.alert.show_stack("Errore", "Riprova più tardi");
                    }
                },
                    (error) => {
                        this.loading.dismiss();
                        console.log("on register.ts", error.error);
                        if (error.status === 400) {
                            for (let key of Object.keys(error.error)) {
                                this.alert.show_stack(key,
                                    error.error[key][0]);
                            }
                        } else {
                            this.alert.show_stack("errore", error.statusText);
                        }
                    });
        } else {
            this.submit_attempt = true;
            this.email_err_submit = true;
            this.pass_err_submit = true;
            this.register_form.controls
                .email.valueChanges.subscribe(() => {
                    this.email_err_submit = false;
                });
            this.register_form.controls
                .passwords.valueChanges.subscribe(() => {
                    this.pass_err_submit = false;
                });
        }
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad RegisterPage");
    }

    password_do_not_match() {
        let pwd_validator_not_valid = this.register_form
            .controls.passwords.invalid;
        let submit_attempt = this.submit_attempt;
        let pass_err_submit = this.pass_err_submit;
        let casted_form_group = this.register_form
            .controls.passwords as FormGroup;
        let first_pwd_valid = casted_form_group.controls.password.valid;

        let condition = (
            pwd_validator_not_valid
            && submit_attempt
            && pass_err_submit
            && first_pwd_valid
        );

        return condition;
    }

}
