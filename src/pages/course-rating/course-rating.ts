import { Component, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
    Events,
    IonicPage,
    Loading,
    LoadingController,
    NavController,
    NavParams,
    ViewController
} from "ionic-angular";
import _ from "lodash";
import { BehaviorSubject } from "rxjs";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { Course } from "../../model/course/course.model";
import { CourseRating } from "../../model/rating/rating.model";
import { CourseProvider } from "../../providers/course/course.provider";
import { RatingDataProvider } from "../../providers/data/rating-data.provider";

@IonicPage()
@Component({
    selector: "page-course-rating",
    templateUrl: "course-rating.html",
})
export class CourseRatingPage {

    private selected_course: BehaviorSubject<Course>;
    private ratings_subject: BehaviorSubject<any>;
    private existing_rating: CourseRating;
    private loading: Loading;

    private rating_form: FormGroup;

    private submit_attempt: boolean = false;

    @ViewChild("stars")
    private stars: any;

    private academic_years = [
        "2018/2019",
        "2017/2018",
        "2016/2017",
        "2015/2016",
    ];

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public view_ctrl: ViewController,
        public form_builder: FormBuilder,
        public events: Events,
        private loading_ctrl: LoadingController,
        public course_provider: CourseProvider,
        public rating_provider: RatingDataProvider) {

        this.rating_form = form_builder.group({
            cfu: ["", Validators.required],
            academic_year: [this.academic_years[0], Validators.required],
            comment: [""]
        });

        this.events.subscribe("refreshTokenExpired",
            () => {
                this.view_ctrl.dismiss();
            });
    }

    ngOnInit() {
        this.selected_course = this.nav_params.get("course");

        let course = this.selected_course.value;
        this.selected_course.next(course);

        if (course.available_cfus.length === 1) {
            this.rating_form.controls.cfu.setValue(course.cfu);
        }
        this.selected_course.subscribe((_course) => {
            console.log("corso in course rating: ", _course);
        });

        this.ratings_subject = this.nav_params.get("rate_sbj");
        this.existing_rating = this.nav_params.get("rating");
        if (this.existing_rating) {
            this.rating_form.controls
                .academic_year.setValue(this.existing_rating.academic_year);
            this.rating_form.controls
                .cfu.setValue(this.existing_rating.cfu);
        }
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

    save_rating() {
        if (this.rating_form.valid) {
            this.show_loading();
            let response = this.existing_rating ?
                this.update_rating() : this.create_new_rating();
            response.subscribe((resp) => {
                this.loading.dismiss();
                this.rating_provider.update_course_ratings(resp);
                this.view_ctrl.dismiss();
            });
        } else {
            this.submit_attempt = true;
        }
    }

    update_rating(): Observable<any> {
        let current_ratings = this.ratings_subject.value;
        let current_rating_idx = _.findIndex(current_ratings,
            { owner: this.existing_rating.owner });
        if (current_rating_idx < 0) {
            throw new Error("owner inesistente");
        }
        // rate-update
        let new_stars = this.stars.rating;
        let ratings_num = current_ratings.length;
        let new_mean = ((this.selected_course.value.stars * ratings_num) +
            new_stars - this.existing_rating.stars) / ratings_num;
        let new_course = this.selected_course.value;
        new_course.stars = new_mean;

        // store change
        this.existing_rating.comment = this.rating_form.controls.comment.value;
        this.existing_rating.stars = new_stars;
        this.existing_rating.cfu = this.rating_form
            .controls.cfu.value.toString(); // FIXME
        this.existing_rating.academic_year = this.rating_form
            // this.year //this.academic_year.value;
            .controls.academic_year.value;
        current_ratings[current_rating_idx] = this.existing_rating;
        return this.course_provider
            .update_course_rating(this.existing_rating)
            .pipe(tap(() => {
                this.selected_course.next(new_course);
                this.ratings_subject.next(current_ratings);
            }
            ));
    }

    create_new_rating(): Observable<any> {
        let new_rate = new CourseRating({
            stars: this.stars.rating,
            comment: this.rating_form.controls.comment.value,
            cfu: this.rating_form.controls.cfu.value.toString(),
            academic_year: this.rating_form.controls.academic_year.value,

        });
        // rate-update
        let new_stars = this.stars.rating;
        let current_ratings = this.ratings_subject.value;
        let ratings_num = current_ratings.length;
        let new_mean = ((this.selected_course.value.stars * ratings_num)
            + new_stars) / (ratings_num + 1);
        let new_course = this.selected_course.value;
        new_course.stars = new_mean;

        return this.course_provider
            .save_course_rating(this.selected_course.value.id, new_rate)
            .pipe(tap((resp) => {
                this.selected_course.next(new_course);
                const current_ratings = this.ratings_subject.value;
                const updated_ratings = [...current_ratings, resp];
                this.ratings_subject.next(updated_ratings);
                new_rate.course = this.selected_course.value.title;
            }
            ));
    }

    close() {
        this.view_ctrl.dismiss();
    }

    delete_rating() {
        this.view_ctrl.dismiss(true);
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad CourseRatingPage");
    }

}
