import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { StarRatingModule } from "ionic3-star-rating";

import { PipesModule } from "../../pipes/pipes.module";

import { CourseRatingPage } from "./course-rating";

@NgModule({
  declarations: [
    CourseRatingPage,
  ],
  imports: [
    IonicPageModule.forChild(CourseRatingPage),
    StarRatingModule,
    PipesModule,
  ],
})
export class CourseRatingPageModule {}
