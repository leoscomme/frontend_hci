import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
    AlertController,
    IonicPage,
    Loading,
    LoadingController,
    NavController,
    NavParams
} from "ionic-angular";
import { Events } from "ionic-angular";

import { AlertProvider } from "../../providers/alert/alert.provider";
import { AuthProvider } from "../../providers/auth/auth.provider";
import { CdlDataProvider } from "../../providers/data/cdl-data.provider";
import { PdsDataProvider } from "../../providers/data/pds-data.provider";
import { RatingDataProvider } from "../../providers/data/rating-data.provider";
import { PdsProvider } from "../../providers/pds/pds.provider";
// import { HomePage } from "../home/home";
// import { RegisterPage } from "../register/register";

@IonicPage()
@Component({
    selector: "page-login",
    templateUrl: "login.html",
})
export class LoginPage {

    loading: Loading;
    credentials = { username: "", password: "" };
    submit_attempt: boolean = false;
    login_form: FormGroup;

    constructor(
        public nav_ctrl: NavController,
        public nav_params: NavParams,
        public form_builder: FormBuilder,
        private auth_provider: AuthProvider,
        private loading_ctrl: LoadingController,
        private alert_ctrl: AlertController,
        private events: Events,
        private alert: AlertProvider,
        private rating_provider: RatingDataProvider,
        public cdl_provider: CdlDataProvider,
        public pds_data_provider: PdsDataProvider,
        public pds_provider: PdsProvider) {
        this.login_form = form_builder.group({
            username: ["", Validators.required],
            password: ["", Validators.required]
        });
    }

    public login() {
        this.show_loading();
        if (this.login_form.valid) {
            this.credentials.username = this.credentials.username.split("@")[0];
            this.auth_provider.login(this.credentials)
                .subscribe((response) => {
                    if (response) {
                        this.events.publish("login");
                        this.loading.dismiss();
                        this.nav_ctrl.setRoot("HomePage", {},
                            {
                                animate: true,
                                animation: "md-transition",
                                direction: "forward",
                                duration: 250,
                                easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
                            });
                        this.auth_provider.set_token(response.access);
                        localStorage.setItem("refresh_token",
                            response.refresh);
                    } else {
                        this.loading.dismiss();
                        this.alert.show_stack("Errore", "Riprova più tardi.");
                    }
                },
                    (error) => {
                        this.loading.dismiss();
                        if (error.status === 400) {
                            // this.showError("Username o
                            // Password non corrette");
                            this.alert.show_stack("Attenzione",
                                "Username o Password non corrette");

                        } else {
                            // this.loading.dismiss();
                            this.alert.show_stack("Errore", error.statusText);
                        }

                    });
        } else {
            this.submit_attempt = true;
        }
    }

    public create_account() {
        this.nav_ctrl.push("RegisterPage");
    }

    show_loading() {
        this.loading = this.loading_ctrl.create({
            content: "Solo un attimo...",
        });
        this.loading.present();
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad LoginPage");
    }

    // ionViewWillEnter() {
    //     this.auth_provider.clear_tokens();
    //     this.rating_provider.clear_local_ratings();
    //     this.cdl_provider.clear_current_cdl();
    //     this.pds_data_provider.clear_pds();
    // }

}
