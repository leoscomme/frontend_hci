import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "truncate"
})
export class TruncatePipe implements PipeTransform {
    // ivan: funziona così: {{value | pipename:args}}
    transform(value: string, args: string): string {
        // console.log("pipe: args=" + args)
        let limit = args ? parseInt(args, 10) : 10;
        // console.log('limit in pipe: ' + limit)
        let trail = args.length > 1 ? args[1] : "...";

        return value.length > limit ? value.substring(0, limit) + trail : value;
    }
}
