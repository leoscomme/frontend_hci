import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import _ from "lodash";
import { Observable } from "rxjs";

import { Cdl } from "../../model/cdl/cdl.model";
import { Course } from "../../model/course/course.model";
import { BaseProvider } from "../abstract-provider";

@Injectable()
export class CdlProvider extends BaseProvider {

    private cdls_url: string = "cdls";

    constructor(public http: HttpClient) {
        super();
    }

    get_all_cdls() {
        return this.http
            .get(this.cdls_url)
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    get_cdl(id: number, academic_year: string): Observable<Cdl> {
        let params = { academic_year: academic_year };
        return this.http
            .get<Cdl>(this.url_join(this.cdls_url, id.toString()),
                { params: params })
            .map((response) => {
                let transformed_courses: Course[] = [];  // result

                // Iterate the response and merge courses
                for (let current_course of response.courses) {

                    let curr_index = _.findIndex(transformed_courses,
                        ["title", current_course.title]);

                    // The course was not already processed: it may still be a
                    // double course, but we don't know it yet. Add it to the
                    // array and populate available_cfus/available_codes with
                    // code/cfu. Set cfu and code to undefined.
                    if (curr_index < 0) {
                        let available_cfus = [current_course.cfu];
                        let available_codes = [current_course.code];
                        let available_ids = [current_course.id];
                        current_course.available_cfus = available_cfus;
                        current_course.available_codes = available_codes;
                        current_course.available_ids = available_ids;
                        current_course.cfu = undefined;
                        current_course.code = undefined;
                        current_course.id = undefined;
                        transformed_courses.push(current_course);
                    } else {
                        transformed_courses[curr_index].
                            available_cfus.push(current_course.cfu);
                        transformed_courses[curr_index].
                            available_codes.push(current_course.code);
                        transformed_courses[curr_index].
                            available_ids.push(current_course.id);
                    }

                }
                // console.log(transformed_courses);
                response.courses = transformed_courses;
                return response;
            })
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }
}
