import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Instructor } from "../../model/instructor/instructor.model";
import { InstructorRating } from "../../model/rating/rating.model";
import { BaseProvider } from "../abstract-provider";

@Injectable()
export class InstructorProvider extends BaseProvider {

    instructors_url: string = "instructors";
    instructor_ratings_url: string = "instructor-ratings";

    constructor(private http: HttpClient) {
        super();
    }

    get_instructor(id: number):
        Observable<Instructor> {
        return this.http
            .get(this.url_join(this.instructors_url, id.toString()))
            .map((response) =>
                new Instructor(response))
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    get_ratings_from_instructor_id(id):
        Observable<InstructorRating[]> {
        return this.http
            .get<InstructorRating[]>(this.instructor_ratings_url,
                { params: { instructor: id.toString() } })
            .map((response) =>
                response.map((rating) => new InstructorRating(rating)))
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    get_instructors_from_cdl(cdl_id):
        Observable<Instructor[]> {
        return this.http
            .get<Instructor[]>(this.instructors_url,
                { params: { cdl: cdl_id.toString() } })
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    save_instructor_rating(instructor_id: number,
        instructor_rating: InstructorRating): Observable<any> {
        return this.http
            .post(this.instructor_ratings_url, instructor_rating,
                { params: { instructor: instructor_id.toString() } })
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }

    update_instructor_rating(instructor_rating: InstructorRating):
        Observable<any> {
        return this.http
            .patch(this.url_join(
                this.instructor_ratings_url, instructor_rating.id.toString()),
                instructor_rating)
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }

    delete_instructor_rating(rating_id: number):
        Observable<any> {
        return this.http.delete(this.url_join(
            this.instructor_ratings_url, rating_id.toString()))
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }

}
