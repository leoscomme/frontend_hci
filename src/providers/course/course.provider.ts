import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Course } from "../../model/course/course.model";
import { CourseRating } from "../../model/rating/rating.model";
import { BaseProvider } from "../abstract-provider";

@Injectable()
export class CourseProvider extends BaseProvider {

    courses_url: string = "courses";
    course_ratings_url: string = "course-ratings";

    constructor(public http: HttpClient) {
        super();
    }

    get_course(id: number): Observable<Course> {

        return this.http
            .get(this.url_join(this.courses_url, id.toString()))
            .map((response) => new Course(response))
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    get_ratings_from_course_id(id): Observable<CourseRating[]> {
        return this.http
            .get<CourseRating[]>(this.course_ratings_url,
                { params: { course: id.toString() } })
            .map((response) =>
                response.map((rating) => new CourseRating(rating)))
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    save_course_rating(course_id: number,
        course_rating: CourseRating): Observable<any> {
        console.log("gli input sono: course_id e course_rating",
            course_id, course_rating);
        return this.http
            .post(this.course_ratings_url, course_rating,
                { params: { course: course_id.toString() } })
            .catch((err) => {
                console.log("occhio!! errore in save provider");
                console.log(err);
                return Observable.throw(err);
            });
    }

    update_course_rating(course_rating: CourseRating): Observable<any> {
        return this.http
            .patch(this.url_join(
                this.course_ratings_url, course_rating.id.toString()),
                course_rating)
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }

    delete_course_rating(rating_id: number): Observable<any> {
        return this.http.delete(this.url_join(
            this.course_ratings_url, rating_id.toString()))
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }

}
