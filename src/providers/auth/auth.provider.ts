import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import "rxjs/add/operator/catch";
import { filter, take } from "rxjs/operators";

import { BaseProvider } from "../abstract-provider";

class TokenResponse {
    public access;
    public refresh;
}

@Injectable()
export class AuthProvider extends BaseProvider {

    readonly token: Observable<string>;
    readonly refresh_token: Observable<any>;

    private simple_jwt_login_url = "api/token";
    private simple_jwt_refresh_url = "api/token/refresh";
    private simple_jwt_verify_url = "api/token/verify";

    private jwt_login_url = "api-token-auth";
    private jwt_refresh_url = "api-token-refresh";
    private jwt_verify_url = "api-token-verify";
    private rest_auth_url = "rest-auth";

    private subject = new BehaviorSubject<string>(undefined);

    constructor(private http: HttpClient) {
        super();
        this.refresh_token = Observable.defer(
            () => {
                this.subject.next(undefined);
                // Next, we refresh the token from the server.
                return this.refreshToken()
                    // Set it as the active token.
                    .do((token) => this.subject.next(token))
                    // Drop the value, ensuring this Observable
                    // only completes when done and doesn't emit.
                    .ignoreElements()
                    // Finally, share the Observable so we don't
                    // attempt multiple refreshes at once.
                    .shareReplay();
            });

        // token, when subscribed, returns the latest token.
        this.token = this
            // Read the subject (stream of tokens).
            .subject
            .pipe(
                filter((_token) => _token !== undefined),
                take(1)
            );

    }

    login(credentials): Observable<TokenResponse> {
        if (credentials.email === null
            || credentials.password === null) {
            return Observable.throw("Please insert credentials");
        } else {
            // TODO fare lo split in caso di email
            return this.http.post<TokenResponse>(
                this.simple_jwt_login_url,
                credentials)
                .map((response) =>
                    response)
                .catch((err) => {
                    console.error(err);
                    return Observable.throw(err);
                });
        }
    }

    register(email: string, passwords): Observable<Object> {
        let body = {
            username: email.split("@")[0],
            email: email,
            password1: passwords.password,
            password2: passwords.confirm_password
        };
        return this.http.post(
            this.rest_auth_url + "/registration/",
            body)
            .map((response) =>
                response)
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    public set_token(new_token: string) {
        this.subject.next(new_token);
    }

    public get_token(): Observable<string> {
        return this
            // Read the subject (stream of tokens).
            .subject
            .pipe(
                filter((_token) => _token !== undefined),
                take(1)
            );

    }

    public clear_tokens() {
        this.subject.next(undefined);
        localStorage.removeItem("refresh_token");
    }

    private get_refresh_token(): string {
        return localStorage.getItem("refresh_token");
    }

    private refreshToken(): Observable<string> {
        const refresh_token = this.get_refresh_token();
        return this.http.post<TokenResponse>(
            this.simple_jwt_refresh_url,
            { refresh: refresh_token })
            .map((response) =>
                response.access)
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });

    }
}
