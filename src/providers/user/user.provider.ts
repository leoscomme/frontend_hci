import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { BaseProvider } from "../abstract-provider";

@Injectable()
export class UserProvider extends BaseProvider {

    user_url: string = "users";
    course_rating_url: string = "course-ratings";
    instructor_rating_url: string = "instructor-ratings";

    constructor(public http: HttpClient) {
        super();
    }

    get_user_info() {
        return this.http
            .get(this.user_url)
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }

    get_user_course_rating() {
        return this.http
            .get(this.course_rating_url,
                { params: { owner: "" } })
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }

    get_user_instructor_rating() {
        return this.http
            .get(this.instructor_rating_url,
                { params: { owner: "" } })
            .catch((err) => {
                console.log(err);
                return Observable.throw(err);
            });
    }
}
