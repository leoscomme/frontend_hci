import urljoin from "url-join";

export class BaseProvider {
    url_join;

    constructor() {
        this.url_join = urljoin;
    }
}
