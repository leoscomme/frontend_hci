import { Injectable } from "@angular/core";
import { AlertController } from "ionic-angular";

/**
 * A simple alert class to show only one alert at the same time
 */
@Injectable()
export class AlertProvider {

    private current_alert: string;
    private alert_options_stack: any[];

    constructor(private alert_ctrl: AlertController) {
        this.alert_options_stack = new Array();
        this.current_alert = undefined;
    }

    public show_stack(title, message,
        buttons: any = [], inputs: any = [], cssClass = "") {
        if (!buttons.length) {
            buttons.push("Ok");
        }
        let alert_options: any = {
            title: title,
            subTitle: message,
            buttons: buttons,
            cssClass: buttons.length === 2 ? "confirmAlert" : cssClass
        };
        if (inputs.length) {
            alert_options.inputs = inputs;
        }

        this.insert_alert(alert_options);

        if (!this.current_alert) {
            let next_config = this.alert_options_stack.shift();
            let alert = this.show(next_config);
            this.current_alert = next_config.subTitle;
            return alert;
        }

    }

    private show(alert_options) {
        let alert = this.alert_ctrl.create(alert_options);
        this.current_alert = alert_options.subTitle;
        alert.present();
        alert.onDidDismiss(() => {
            console.log("internal onDidDismiss");
            if (this.alert_options_stack.length > 0) {
                this.show(this.alert_options_stack.shift());
            } else {
                this.current_alert = undefined;
            }
        });
        return alert;
    }

    private insert_alert(new_alert_opt): boolean {
        if (this.current_alert === new_alert_opt.subTitle) {
            return false;
        }
        for (let alert_opt of this.alert_options_stack) {
            if (alert_opt.subTitle === new_alert_opt.subTitle) {
                return false;
            }
        }
        this.alert_options_stack.push(new_alert_opt);
        return true;
    }
}
