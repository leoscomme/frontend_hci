import { Injectable } from "@angular/core";
import _ from "lodash";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

import {
    CourseRating,
    InstructorRating
} from "../../model/rating/rating.model";
import { UserProvider } from "../user/user.provider";

@Injectable()
export class RatingDataProvider {

    private course_rating_source =
        new BehaviorSubject<CourseRating[]>(undefined);
    private course_rating_collection = this.course_rating_source
        .asObservable().pipe(
            map((val) => _.cloneDeep(val))
        );
    private instructor_rating_source =
        new BehaviorSubject<InstructorRating[]>(undefined);
    private instructor_rating_collection = this.instructor_rating_source
        .asObservable().pipe(
            map((val) => _.cloneDeep(val))
        );

    public constructor(public user_provider: UserProvider) {

    }

    public clear_local_ratings() {
        this.course_rating_source.next(undefined);
        this.instructor_rating_source.next(undefined);
    }

    public get_instructor_ratings(): Observable<InstructorRating[]> {
        if (!this.instructor_rating_source.value) {
            this.user_provider.get_user_instructor_rating()
                .subscribe((resp) => {
                this.instructor_rating_source.next(resp);
            });
        }
        return this.instructor_rating_collection;
    }

    public get_rating_from_instructor_title(instructor_title: string) {
        if (!this.instructor_rating_source.value) {
            this.user_provider.get_user_instructor_rating()
                .subscribe((resp) => {
                this.instructor_rating_source.next(resp);
            });
        }
        return this.instructor_rating_collection.pipe(
            map((_instructor_ratings) =>
                _.find(_instructor_ratings, { instructor: instructor_title }))
        );
    }

    public update_instructor_ratings(_rating: InstructorRating) {
        let current_rating_list = this.instructor_rating_source.getValue();

        let len = current_rating_list.length;

        if (len === 0) {
            // First instructor rating created:
            // simply puch it in the empty list
            current_rating_list.push(_rating);
        } else if (len === 1) {
            // Handle special case of single rating in list.
            // Cannot be done with array methods from lodash
            current_rating_list[0] = _rating;
        } else {
            // Here the list has at least two elements.
            // Default behaviour using array methods.
            // Find index of _rating in list
            let index = _.findIndex(current_rating_list, ["id", _rating.id]);
            if (index >= 0) { // rating is already in current rating list
                current_rating_list[index] = _rating;
            } else {
                current_rating_list.push(_rating);
            }
        }
        this.instructor_rating_source.next(current_rating_list);
    }

    public delete_instructor_rating(_rating: InstructorRating) {
        let current_rating_list = this.instructor_rating_source.getValue();
        let index = _.findIndex(current_rating_list, ["id", _rating.id]);
        if (index >= 0) {
            _.pullAt(current_rating_list, index);
        }
        this.instructor_rating_source.next(current_rating_list);
    }

    public get_course_ratings(): Observable<CourseRating[]> {
        if (!this.course_rating_source.value) {
            this.user_provider.get_user_course_rating().subscribe((resp) => {
                this.course_rating_source.next(resp);
            });
        }
        return this.course_rating_collection;
    }

    public get_rating_from_course_title(course_title: string) {
        console.log("course_rating_source: ", this.course_rating_source);
        if (!this.course_rating_source.value) {
            this.user_provider.get_user_course_rating().subscribe((resp) => {
                this.course_rating_source.next(resp);
            });
        }
        return this.course_rating_collection.pipe(
            map((_course_ratings) =>
                _.find(_course_ratings, { course: course_title }))

        );
    }

    public update_course_ratings(_rating: CourseRating) {
        let current_rating_list = this.course_rating_source.getValue();

        let len = current_rating_list.length;
        console.log("current rating list ", current_rating_list);
        console.log("rating che arriva a update", _rating);
        if (len === 0) {
            // First instructor rating created:
            // simply push it in the empty list
            current_rating_list.push(_rating);
        } else if (len === 1) {
            // Handle special case of single rating in list.
            // Cannot be done with array methods from lodash
            current_rating_list[0] = _rating;
        } else {
            // Here the list has at least two elements.
            // Default behaviour using array methods.
            // Find index of _rating in list
            let index = _.findIndex(current_rating_list,
                ["course", _rating.course]);
            if (index >= 0) { // rating is already in current rating list
                current_rating_list[index] = _rating;
            } else {
                current_rating_list.push(_rating);
            }
        }
        this.course_rating_source.next(current_rating_list);
    }

    public delete_course_rating(_rating: CourseRating) {
        let current_rating_list = this.course_rating_source.getValue();
        let index = _.findIndex(current_rating_list, ["id", _rating.id]);
        if (index >= 0) {
            _.pullAt(current_rating_list, index);
        }
        this.course_rating_source.next(current_rating_list);
    }

}
