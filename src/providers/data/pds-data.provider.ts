import { Injectable } from "@angular/core";
import _ from "lodash";
import { BehaviorSubject, Observable } from "rxjs";
import { filter, map, tap } from "rxjs/operators";

import { Pds } from "../../model/pds/pds.model";
import { PdsProvider } from "../pds/pds.provider";

/*
Usage:
1) import PdsDataProvider in Component
2) subscribe to get_current_pds in ngOnInit and save value in local variable
3) create method save and update that call PdsDataProvider.save_pds()
   or update_pds() with the local variable
3) modify local variable as needed
4) call created methods to automatically save changes in backend and propagate
   them to every other component that subscribed to this class
*/

@Injectable()
export class PdsDataProvider {

    public pds_source = new BehaviorSubject<Pds>(undefined);
    private current_pds = this.pds_source.asObservable().pipe(
        filter((val) => val !== undefined),
        map((val) => _.cloneDeep(val))
    );

    private pds_list_source = new BehaviorSubject<Pds[]>(undefined);
    private pds_list = this.pds_list_source.asObservable().pipe(
        filter((val) => val !== undefined),
        map((val) => _.cloneDeep(val))
    );

    public constructor(public pds_provider: PdsProvider) {
        this.pds_provider.get_pds_list()
            .pipe(
                tap((_pds_list) => {
                    this.set_pds_list(_pds_list);
                    this.set_current_pds(_pds_list[0]);
                }),
        ).subscribe();
    }

    public get_current_pds(): Observable<Pds> {
        return this.current_pds;
    }

    public clear_pds() {
        this.pds_source = new BehaviorSubject<Pds>(undefined);
        this.current_pds = this.pds_source.asObservable().pipe(
            filter((val) => val !== undefined),
            map((val) => _.cloneDeep(val))
        );

        this.pds_list_source = new BehaviorSubject<Pds[]>(undefined);
        this.pds_list = this.pds_list_source.asObservable().pipe(
            filter((val) => val !== undefined),
            map((val) => _.cloneDeep(val))
        );
    }

    public set_current_pds(_pds: Pds) {

        this.pds_source.next(_pds);
        // console.log("pds_source name updated: ",
        // this.pds_source.getValue().name);
        let current_pds_list = this.pds_list_source.getValue();

        let len;
        if (current_pds_list === undefined) {
            len = 0;
            current_pds_list = [];
        }
        else {
            len = current_pds_list.length;
        }

        if (len === 0) {
            // First pds created: simply push it in the empty list.
            current_pds_list.push(_pds);
        } else if (len === 1) {
            // Handle special case of single pds in list. Cannot be done with
            // array methods from lodash

            // If the pds being updated is the same, replace it (it is in
            // the 0-th position as len===1)
            if (current_pds_list[0].id === _pds.id) {
                current_pds_list[0] = _pds;
            }
            // Else, add the new pds to the list
            else {
                current_pds_list.unshift(_pds);
            }

        } else {
            // Here the list has at least two elements.
            // Default behaviour using array methods.
            // Find index of _pds in list
            let index = _.findIndex(current_pds_list, ["id", _pds.id]);
            // console.log("index: ", index);
            if (index >= 0) {  // _pds is already in current_pds_list
                // remove el at index and shift it in first position
                _.pullAt(current_pds_list, index);
                // console.log("pulled.name: ", pulled.name);
                // console.log("current_pds_list before unshift:");
                // console.log(current_pds_list);
                current_pds_list.unshift(_pds);
                // console.log("current_pds_list after unshift:");
                // console.log(current_pds_list);
            } else {
                // _pds has been created from scratch,
                // simply add it in first position
                current_pds_list.unshift(_pds);
            }
        }

        // In any case update the resulting new list.
        this.pds_list_source.next(current_pds_list);
        // console.log("pds_list_source at the end: ");
        // console.log(this.pds_list_source.getValue());
    }

    public get_pds_list(): Observable<Pds[]> {
        return this.pds_list;
    }
    public set_pds_list(_pds_list: Pds[]) {
        this.pds_list_source.next(_pds_list);
    }

    public save_pds(pds: Pds) {
        this.pds_provider.save_pds(pds).subscribe(
            (created_pds) => {
                console.log("####### created_pds #######");
                console.log(created_pds);
                // console.log(pds)
                this.set_current_pds(created_pds);

            }
        ); // TODO: handle errors..
    }

    public update_pds(updated_pds: Pds) {
        console.log("PdsDataProvider::update_pds");
        let current_pds: Pds;
        let modified_fields = { id: undefined };

        current_pds = this.pds_source.getValue();
        // console.log("updated_pds.courses: ", updated_pds.courses);
        // console.log("current_pds.courses: ", current_pds.courses);

        for (let prop of Object.keys(updated_pds)) {
            if (!_.isEqual(updated_pds[prop], current_pds[prop])) {
                // console.log("prop not equal: ", prop);
                // console.log(typeof prop)
                // console.log(updated_pds[prop]);
                // console.log(current_pds[prop]);
                if (prop === "courses") {
                    // console.log("prop == courses");
                    modified_fields[prop] = new Array<number>(
                        updated_pds[prop].length);
                    for (let i in updated_pds[prop]) {
                        modified_fields[prop][i] = updated_pds[prop][i].id;
                    }
                } else {
                    modified_fields[prop] = updated_pds[prop];
                }
            }
        }
        // console.log("modified_fields: ", modified_fields);
        // console.log("is empty: ", _.isEmpty(modified_fields));
        if (_.isEmpty(modified_fields)) {
            console.log("NOT MODIFIED.");
        } else {
            // console.log("modified_fields:\n", modified_fields);
            modified_fields.id = current_pds.id;
            this.pds_provider.update_pds(modified_fields)
                .subscribe();  // TODO: handle errors..
            // console.log("PdsDataProvider::update_pds -> pds_source.next");
            this.set_current_pds(updated_pds);
            // console.log("PdsDataProvider::update_pds >>> FINISHED");
            // console.log("********************** END **********************");
        }
    }

    public rename_pds_from_id(id: number, name: string) {
        let current_pds_list = this.pds_list_source.getValue();
	let index = _.findIndex(current_pds_list, ["id", id]);
	if(index >= 0 && index < current_pds_list.length){
	    current_pds_list[index].name = name
	    this.pds_list_source.next(current_pds_list);
	}else{
	    // handle with any exception
	}
    }

    
    public delete_pds(id: number) {
        let current_pds_list = this.pds_list_source.getValue();
        console.log("delete::current_pds_list before",
            _.cloneDeep(current_pds_list));
        _.remove(current_pds_list, { id: id });
        console.log("delete::current_pds_list after", current_pds_list);
        this.set_pds_list(current_pds_list);
        if (current_pds_list.length === 0) {
            // localStorage.setItem("at_least_one_pds", "false");
            this.pds_provider.at_least_one_pds.next(false);
            this.pds_source.next(undefined);
        }
        else {
            this.pds_source.next(current_pds_list[0]);
            this.pds_list_source.next(current_pds_list);
        }
        this.pds_provider.delete_pds(id).subscribe();
    }
}
