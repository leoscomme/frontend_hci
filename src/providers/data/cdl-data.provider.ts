import { Injectable } from "@angular/core";
import _ from "lodash";
import { BehaviorSubject, Observable } from "rxjs";
import { distinctUntilChanged, filter, map, tap } from "rxjs/operators";

import { Cdl } from "../../model/cdl/cdl.model";
import { CdlProvider } from "../cdl/cdl.provider";

import { PdsDataProvider } from "./pds-data.provider";

/*
- cdl is taken from the current pds.
- TODO: usare distinctUntilChanged di rxjs? ==> ✓
- TODO: switchMap
*/

@Injectable()
export class CdlDataProvider {

    private cdl_source = new BehaviorSubject<Cdl>(undefined);
    private current_cdl = this.cdl_source.asObservable().pipe(
        filter((val) => val !== undefined),
        map((val) => _.cloneDeep(val))
    );

    public constructor(
        public cdl_provider: CdlProvider,
        public pds_provider: PdsDataProvider) {
        // FIXMEEEEE: questo va fatto con switchmap o
        // qualche altra diavoleria del genere
        this.pds_provider.get_current_pds().pipe(
            map((_pds) => _pds.cdl),
            distinctUntilChanged(),
        ).
            subscribe((_cdl_id) => {
                // FIXME: Da dove lo prendiamo questo?
                this.cdl_provider.get_cdl(_cdl_id, "2017/2018")
                    .pipe(
                        tap((_cdl) => {
                            // console.log("CdlDataProvider: updating cdl");
                            // console.log(_cdl);
                            this.set_current_cdl(_cdl);
                        }),
                ).subscribe();
            });
    }

    public get_current_cdl(): Observable<Cdl> {
        return this.current_cdl;
    }

    public set_current_cdl(_cdl: Cdl) {
        this.cdl_source.next(_cdl);
    }

    public clear_current_cdl() {
        this.cdl_source = new BehaviorSubject<Cdl>(undefined);
        this.current_cdl = this.cdl_source.asObservable().pipe(
            filter((val) => val !== undefined),
            map((val) => _.cloneDeep(val))
        );
    }

    public save_cdl() {
        throw new Error("Not implemented");
    }

    public update_cdl() { // updated_cdl: Cdl
        throw new Error("Not implemented");
    }
}
