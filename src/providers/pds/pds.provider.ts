import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import _ from "lodash";
import { BehaviorSubject, Observable } from "rxjs";
import { catchError } from "rxjs/operators";

import { Pds } from "../../model/pds/pds.model";
import { BaseProvider } from "../abstract-provider";

@Injectable()
export class PdsProvider extends BaseProvider {
    public at_least_one_pds: BehaviorSubject<boolean>;

    private pds_url = "pds";

    constructor(public http: HttpClient) {
        super();
        this.at_least_one_pds = new BehaviorSubject(null);
    }

    get_pds(id: number): Observable<Pds> {
        return this.http
            .get<Pds>(this.url_join(this.pds_url, id.toString()))
            .map((response) => new Pds(response))
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    get_pds_list(): Observable<Pds[]> {
        return this.http
            .get<Pds[]>(this.pds_url)
            .do((val) => {
                if (val.length > 0) {
                    console.log("get_pds_list -> val.length > 0");
                    // localStorage.setItem("at_least_one_pds", "true");
                    this.at_least_one_pds.next(true);
                }
                else {
                    console.log("get_pds_list -> val.length <= 0");
                    // localStorage.setItem("at_least_one_pds", "false");
                    this.at_least_one_pds.next(false);
                }
            })
            .filter((val) => val.length !== 0)
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

    update_pds(new_values: { id }) {
        console.log("PdsProvider::update_pds");
        return this.http
            .patch(this.url_join(this.pds_url,
                new_values.id.toString()), _.omit(new_values, ["id"]))
            .pipe(
                catchError((err) => {
                    console.error(err);
                    return Observable.throw(err);
                }),
        );
    }

    save_pds(pds) {
        console.log("PdsProvider::save_pds");
        // let pds_string = this.pds_to_json(pds);
        // console.log("pds_string: ", pds_string);

        // mmmmmmmmhh....
        pds["courses"] = [];
        pds["cdl"] = 1;
        return this.http
            .post(this.pds_url, pds)
            .pipe(
                catchError((err) => {
                    console.error(err);
                    return Observable.throw(err);
                }),
        );
    }

    delete_pds(id: number) {
        return this.http
            .delete(this.url_join(this.pds_url, id.toString()))
            .catch((err) => {
                console.error(err);
                return Observable.throw(err);
            });
    }

}
