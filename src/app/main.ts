import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { AppModule } from "./app.module";

let DEBUG = false;
if (!DEBUG) {
    enableProdMode();
    // let methods = ["log"];
    // for (let method of methods) {
    //     console[method] = function() {};
    // }
} else {
    const debug_string =
        `
    ************************


      RUNNING DEBUG BUILD!


    ************************

    `;

    console.error(debug_string);
}

platformBrowserDynamic().bootstrapModule(AppModule);
