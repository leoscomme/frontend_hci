import { Component, ViewChild } from "@angular/core";
import { Network } from "@ionic-native/network";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { AlertController, MenuController, Nav, Platform } from "ionic-angular";
import { Events } from "ionic-angular";

import { AlertProvider } from "../providers/alert/alert.provider";
import { AuthProvider } from "../providers/auth/auth.provider";
import { CdlDataProvider } from "../providers/data/cdl-data.provider";
import { PdsDataProvider } from "../providers/data/pds-data.provider";
import { RatingDataProvider } from "../providers/data/rating-data.provider";

@Component({
    templateUrl: "app.html"
})
export class MyApp {

    @ViewChild(Nav) nav: Nav;
    rootPage: any = "LoginPage";
    pages: { title: string, component: any }[];
    icon_for_page: string[];
    app_name = "UniFiAdvisor";

    constructor(public platform: Platform,
        public menu: MenuController,
        public status_bar: StatusBar,
        public splash_screen: SplashScreen,
        public events: Events,
        private alert: AlertProvider,
        private alert_ctrl: AlertController,
        private network: Network,
        private rating_provider: RatingDataProvider,
        private auth_provider: AuthProvider,
        public cdl_provider: CdlDataProvider,
        public pds_data_provider: PdsDataProvider) {

        this.events.subscribe("refreshTokenExpired",
            () => {

                this.alert.show_stack("Unauthorized",
                    "Your credentials have expired.  Please log back in.");

                this.nav.setRoot("LoginPage", {},
                    {
                        animate: true,
                        animation: "md-transition",
                        direction: "forward",
                        duration: 250,
                        easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
                    });
            });

        this.initializeApp();

        this.icon_for_page = ["home", "book",
            "school", "search", "exit_to_app"];
        this.pages = [
            { title: "Home", component: "HomePage" },
            { title: "I miei Piani di Studi", component: "PdsPage" },
            { title: "Cerca Docenti", component: "InstructorSearchPage" },
            { title: "Cerca Corsi", component: "CourseSearchPage" },
            { title: "Logout", component: "LoginPage" }
        ];
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // this.status_bar.styleDefault();
            this.status_bar.overlaysWebView(false);
            this.status_bar.backgroundColorByHexString("#001c54");

            this.splash_screen.hide();
            let conntype = this.network.type;

            if (!conntype || conntype === "unknown" || conntype === "none") {
                const alert = this.alert_ctrl.create({
                    title: "Attenzione",
                    message: "Devi essere connesso a Internet per usare l'App.",
                    buttons: [
                        {
                            text: "Ok",
                            handler: () => this.platform.exitApp()
                        }
                    ]
                });
                if (this.platform.is("cordova")) {
                    alert.present();
                }
            }
        });
    }

    openPage(page) {
        this.menu.close();
        if (page.component === "LoginPage") {
            console.log("Logging out...");
            // this.auth_provider.clear_tokens();
            this.rating_provider.clear_local_ratings();
            // this.cdl_provider.clear_current_cdl();
            // this.pds_data_provider.clear_pds();
        }
        this.nav.setRoot(page.component, {},
            {
                animate: true,
                animation: "md-transition",
                direction: "forward",
                duration: 250,
                easing: "cubic-bezier(0.4, 0.0, 0.2, 1)"
            });
    }
}
