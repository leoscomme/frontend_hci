
import { HttpClientModule } from "@angular/common/http";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Network } from "@ionic-native/network";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { StarRatingModule } from "ionic3-star-rating";

import { ComponentsModule } from "../components/components.module";
import { ExpandableComponent } from "../components/expandable/expandable";
import { RequestInterceptor } from "../interceptors/request.interceptor";
import { AlertProvider } from "../providers/alert/alert.provider";
import { AuthProvider } from "../providers/auth/auth.provider";
import { CdlProvider } from "../providers/cdl/cdl.provider";
import { CourseProvider } from "../providers/course/course.provider";
import { CdlDataProvider } from "../providers/data/cdl-data.provider";
import { PdsDataProvider } from "../providers/data/pds-data.provider";
import { RatingDataProvider } from "../providers/data/rating-data.provider";
import {
    InstructorProvider
} from "../providers/instructor/instructor.provider";
import { PdsProvider } from "../providers/pds/pds.provider";
import { UserProvider } from "../providers/user/user.provider";

import { MyApp } from "./app.component";

@NgModule({
    declarations: [
        MyApp,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp),
        StarRatingModule,
        ComponentsModule,
        BrowserAnimationsModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        ExpandableComponent,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        InstructorProvider,
        CourseProvider,
        CdlProvider,
        AuthProvider,
        AlertProvider,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestInterceptor,
            multi: true
        },
        PdsProvider,
        PdsDataProvider,
        CdlDataProvider,
        UserProvider,
        RatingDataProvider,
        Network,
    ]
})
export class AppModule { }
