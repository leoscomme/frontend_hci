import {
    animate,
    state,
    style,
    transition,
    trigger
} from "@angular/animations";
import { Component, Input } from "@angular/core";

@Component({
    selector: "expandable",
    templateUrl: "expandable.html",

    animations: [
        trigger("openClose", [
            state("open", style({
                height: "*",
            })),
            state("closed", style({
                height: "0px",
            })),
            transition("open => closed", [
                animate("250ms cubic-bezier(0.4, 0.0, 0.2, 1)")
            ]),
            transition("closed => open", [
                animate("200ms cubic-bezier(0.4, 0.0, 0.2, 1)")
            ]),
        ]),
    ]
})
export class ExpandableComponent {

    @Input("expanded") expanded;

    ngAfterViewInit() {}

}
