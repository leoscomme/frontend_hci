import { Instructor } from "../instructor/instructor.model";

export class Course {
    id: number;
    available_ids?: number[];
    academic_year: string;
    cdl: string;
    title: string;
    code: string;
    available_codes?: string[];
    department?: string;
    sector?: string;
    teaching_schedule?: string;
    teaching_period?: string;
    cfu?: number;
    available_cfus?: number[];
    mutuazioni?: string;
    content?: string;
    textbooks?: string;
    objectives?: string;
    methods?: string;
    exam?: string;
    program?: string;
    link?: string;
    stars?: number;
    instructors?: Instructor[];

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}
