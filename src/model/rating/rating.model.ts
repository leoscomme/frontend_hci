import { Course } from "../course/course.model";
import { Instructor } from "../instructor/instructor.model";
import { User } from "../user/user.model";

class Rating {
    url?: string;
    id?: number;
    stars: number;
    comment?: string;
    owner?: User;
    created?: Date;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class CourseRating extends Rating {
    course?: string | Course;
    cfu: string;
    academic_year: string;

    constructor(values: CourseRating) {
        super(values);
        Object.assign(this, values);
    }
}

export class InstructorRating extends Rating {
    instructor?: Instructor;

    constructor(values: Object = {}) {
        super(values);
        Object.assign(this, values);
    }
}
