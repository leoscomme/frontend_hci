import { Course } from "../course/course.model";
import { User } from "../user/user.model";

export class Pds {
    id: number;
    name: string;
    courses?: Course[];
    owner: User;
    cdl: number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
