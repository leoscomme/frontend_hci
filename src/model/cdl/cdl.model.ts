import { Course } from "../course/course.model";

export class Cdl {
    id: number;
    name: string;
    courses?: Course[];
}
