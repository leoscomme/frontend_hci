import { Course } from "../course/course.model";

export class Instructor {
    id: number;
    name: string;
    stars?: number;
    courses?: Course[];

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}
