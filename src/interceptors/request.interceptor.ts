import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Events } from "ionic-angular";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import urljoin from "url-join";

import { AuthProvider } from "../providers/auth/auth.provider";

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    base_url: string;
    url_join;

    constructor(public auth: AuthProvider, public event: Events) {
        this.url_join = urljoin;
        this.base_url = "https://stormy-caverns-15453.herokuapp.com/";
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        let base_req = request.clone();
        request = this.addAbsUrl(request);
        if (request.url.includes("registration")) {
            console.log("registrazione tramite interceptor");
            return next.handle(request).do(
                (event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        //
                        console.log("refresh andato a buon fine");
                    }
                });
        }
        else if (request.url.includes("token")) {
            console.log("l'url contiene la keyword token -> ", request);

            return next.handle(request).do(
                (event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        //
                        console.log("refresh andato a buon fine");
                    }
                }, (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        // console.log('errore non 401 -> ', err);
                        // console.log('con request: ', request);
                        if (err.status === 401) {
                            console.log("molto probabilmente è" +
                                " scaduto il token di refresh");
                            this.event.publish("refreshTokenExpired");
                        }
                    }
                }
            );
        } else {
            return this
                .auth
                // Get the latest token from the auth service.
                .token
                // Map the token to a request with the right header set.
                .map((_token) => {
                    const auth_token = ("Bearer " + _token);
                    console.log(_token);
                    request = request.clone({
                        setHeaders: {
                            Authorization: auth_token
                        }
                    });

                    return request;
                })
                // Execute the request on the server.
                .concatMap((authReq) => next.handle(authReq))
                // Catch the 401 and handle it by refreshing the token and
                // restarting the chain (where a new subscription to
                //  this.auth.token will get the latest token).
                .catch((err, restart) => {
                    console.log("ecco cosa vedo: err ->", err);
                    console.log("restart -> ", restart);
                    // If the request is unauthorized,
                    // try refreshing the token before restarting.
                    if (err instanceof HttpErrorResponse &&
                        !(err.status === 401)) {
                        console.log("errore non 401 -> ", err);
                        console.log("con request: ", request);
                    }
                    if (err instanceof HttpErrorResponse &&
                        err.status === 401) {
                        console.log("url dell'errore (401) -> " + err.url);
                        console.log(" in err 401 la request (prima): ",
                            request);

                        let retry = this.auth.refresh_token.map(() =>
                            this.auth.token.map((_token) => {
                                const auth_token = ("Bearer " + _token);
                                request = request.clone({
                                    setHeaders: {
                                        Authorization: auth_token
                                    }
                                });
                            }));
                        // retry.subscribe(resp => {
                        //     console.log('ecco retry cosa regala ', resp)
                        // })
                        retry.subscribe();
                        // return Observable.concat(this.auth.refresh_token,
                        // restart);
                        return Observable.concat(retry, restart);

                    }
                    throw err;
                });
        }
    }

    private addAbsUrl(request: HttpRequest<any>): HttpRequest<any> {
        return request.clone({
            url: this.url_join(this.base_url, request.url, "/")
        });
    }

}
